import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { BuscarEstacionPage } from './pages/buscar-estacion/buscar-estacion.page';
import { IngresaCargaPage } from './pages/ingresa-carga/ingresa-carga.page';
import { DataResolverService } from './pages/resolver/data-resolver';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./pages/registro/registro.module').then( m => m.RegistroPageModule)
  },
  // {
  //   path: 'carga',
  //   loadChildren: () => import('./pages/carga/carga.module').then( m => m.CargaPageModule)
  // },
  {
    path: 'perfil',
    loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'actividad',
    loadChildren: () => import('./pages/actividad/actividad.module').then( m => m.ActividadPageModule)
  },
  {
    path: 'ingresa-carga/:id', component: IngresaCargaPage ,
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./pages/ingresa-carga/ingresa-carga.module').then( m => m.IngresaCargaPageModule)
  },
  {
    path: 'resumen/:id',
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./pages/resumen/resumen.module').then( m => m.ResumenPageModule)
  },
  {
    path: 'buscar-estacion/:id', component: BuscarEstacionPage,
    resolve: {
      special: DataResolverService
    },
    loadChildren: () => import('./pages/buscar-estacion/buscar-estacion.module').then( m => m.BuscarEstacionPageModule)
  },
  {
    path: 'nueva-tarjeta',
    loadChildren: () => import('./pages/nueva-tarjeta/nueva-tarjeta.module').then( m => m.NuevaTarjetaPageModule)
  },
  {
    path: 'nueva-ubicacion',
    loadChildren: () => import('./pages/nueva-ubicacion/nueva-ubicacion.module').then( m => m.NuevaUbicacionPageModule)
  },
  {
    path: 'tarjetas',
    loadChildren: () => import('./pages/tarjetas/tarjetas.module').then( m => m.TarjetasPageModule)
  },
  {
    path: 'mis-autos',
    loadChildren: () => import('./pages/mis-autos/mis-autos.module').then( m => m.MisAutosPageModule)
  },
  {
    path: 'nuevo-auto',
    loadChildren: () => import('./pages/nuevo-auto/nuevo-auto.module').then( m => m.NuevoAutoPageModule)
  },
  {
    path: 'googlemaps',
    loadChildren: () => import('./components/googlemaps/googlemaps.module').then( m => m.GooglemapsPageModule)
  },
  {
    path: 'editar-auto',
    loadChildren: () => import('./pages/editar-auto/editar-auto.module').then( m => m.EditarAutoPageModule)
  },
  {
    path: 'facturacion',
    loadChildren: () => import('./pages/facturacion/facturacion.module').then( m => m.FacturacionPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
