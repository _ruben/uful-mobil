import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { MetodosDePagoService } from '../../services/metodos-de-pago.service';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.page.html',
  styleUrls: ['./tarjetas.page.scss'],
})
export class TarjetasPage implements OnInit {
  changeIndex(index: number) {
    this.metodos.indexSeleccionado = index;
  }

  getTarjeta(){
    return this.metodos.getDefault();
  }

eliminarTarjeta(_t40: { nombreTitular: string; icon: string; terminacion: string; }) {
throw new Error('Method not implemented.');
}
editarTarjeta(_t40: { nombreTitular: string; icon: string; terminacion: string; }) {
throw new Error('Method not implemented.');
}
  nuevaTarjeta() {
    this.router.navigate(['/nueva-tarjeta']);
  }

  tarjetas: any[] = []

  constructor(private router : Router, private metodos : MetodosDePagoService) {
    this.tarjetas = this.metodos.getMetodosDePago();
  }

  ngOnInit() {
  }

}
