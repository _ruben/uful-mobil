import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  showPassword = false;
  typePassword = 'password';
  email: string = '';
  contrasena: string = '';


  async presentToastAlert(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      position: 'top'
    });

    await toast.present();
  }

  login() : boolean {
    if(this.email === ''){
      this.presentToastAlert('El correo es obligatorio');
      return false;
    }
    else if(this.contrasena === ''){
      this.presentToastAlert('La contraseña es obligatoria');
      return false;
    }
    else if(this.contrasena.length < 8){
      this.presentToastAlert('La contraseña debe tener al menos 8 caracteres');
      return false;
    }
    else{
      return this.loginService.postLogin(this.email, this.contrasena);
    }
  }





  constructor(private loginService: LoginService, private toastController: ToastController) {
    this.name = 'Contraseña';
  }

  name: string;



  @Output() contrasenaCambiada: EventEmitter<string> = new EventEmitter();

  ngOnInit() {}


  toogleShowPassword() {
    this.showPassword = !this.showPassword;
    this.typePassword = this.showPassword ? 'text' : 'password';
  }

}
