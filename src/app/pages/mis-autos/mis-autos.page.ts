import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';

import { Auto } from 'src/app/interfaces';
import { AutosService } from 'src/app/services/autos.service';


@Component({
  selector: 'app-mis-autos',
  templateUrl: './mis-autos.page.html',
  styleUrls: ['./mis-autos.page.scss'],
})
export class MisAutosPage {
  setFavorite(_t40: Auto) {
    if(_t40.favorite) return; //ya es favorito
    this.autos.setFavorito(_t40).subscribe(
      res => {
        console.log("Favorito: at mis autos ", res);
        this.obtenerAutos();
      },
      error => {
        console.error(error);
      }
    );
  }

  eliminarAuto(auto : Auto) {
    console.log("Eliminando auto: ", auto);
    if(auto._id !== undefined) this.autos.deleteAuto(auto._id);
    else console.error("No se puede eliminar un auto sin ID");
  }

  nuevoAuto() {
    this.router.navigate(['/nuevo-auto']);
  }

  misAutos: Auto[] = [];
  editarAuto(auto : Auto) {
    console.log("Editar auto: ", auto);
    this.router.navigate(['/editar-auto'], { state: { auto } });
  }

  constructor(private autos: AutosService, private router : Router, private ngZone: NgZone) {
    this.obtenerAutos();
  }

  async ionViewWillEnter() {
    this.obtenerAutos();
  }



  async obtenerAutos() {
    this.autos.getAutos().subscribe(
      autos => {
        this.ngZone.run(() => {
          this.misAutos = autos;
          console.log("Autos: ", autos);
        });
      },
      error => {
        console.error(error);
      }
    );

  }

}
