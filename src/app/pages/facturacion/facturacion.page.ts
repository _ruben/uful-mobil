import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { FacturacionService } from 'src/app/services/facturacion.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.page.html',
  styleUrls: ['./facturacion.page.scss'],
})
export class FacturacionPage implements OnInit {

  async presentErrorAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Error!',
      subHeader: msg,
      buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
    });
    await alert.present();
  }

  guardar() {
    console.log("guardando datos fiscales")
    this.loadingService.showLoading('Guardando datos fiscales');
    if(this.facturacion.rfc ==='') this.presentErrorAlert('El RFC es obligatorio');
    if(this.facturacion.razonSocial ==='') this.presentErrorAlert('La razón social es obligatoria');
    if(this.facturacion.domicilioFiscal ==='') this.presentErrorAlert('El domicilio fiscal es obligatorio');
    if(this.facturacion.regimenFiscal ==='') this.presentErrorAlert('El regimen fiscal es obligatorio');
    if(this.facturacion.usoCFDI ==='') this.presentErrorAlert('El uso CFDI es obligatorio');

      this.facturacionService.postFacturacion(this.facturacion);
      this.loadingService.hideLoading();

  }

  constructor(private facturacionService : FacturacionService, private alertController: AlertController, private loadingService: LoadingService) { }


  // "RFC": "XOJI740919U48",
  // "razonSocial": "XODAR JIMENEZ",
  // "domicilioFiscal": "73000",
  // "regimenFiscal": "612",
  // "usoCFDI": "S01"

  facturacion = {
    rfc: '',
    razonSocial: '',
    domicilioFiscal: '',
    regimenFiscal: '',
    usoCFDI: ''
  };

  ngOnInit() {
  }

}
