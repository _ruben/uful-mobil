import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NuevaTarjetaPage } from './nueva-tarjeta.page';

describe('NuevaTarjetaPage', () => {
  let component: NuevaTarjetaPage;
  let fixture: ComponentFixture<NuevaTarjetaPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(NuevaTarjetaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
