import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AES } from 'crypto-js';
import { Tarjeta } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-nueva-tarjeta',
  templateUrl: './nueva-tarjeta.page.html',
  styleUrls: ['./nueva-tarjeta.page.scss'],
})
export class NuevaTarjetaPage implements OnInit {
submit() {
throw new Error('Method not implemented.');
}
  name: string = '';
  number: string = ''; // este va encriptado
  cvc: string = '';
  expiry: string = '';
  secretNumber: string = ''; // este va enmascarado tipo **** **** **** 1234
  clave: string = '';
  constructor( private alertController: AlertController, private router: Router ) {;
  }

  ngOnInit() {

  }

  encriptaTarjeta() {
    this.number = AES.encrypt(this.number, environment.encryptKey).toString();
    this.cvc = AES.encrypt(this.cvc,environment.encryptKey).toString();
    this.expiry = AES.encrypt(this.expiry,environment.encryptKey).toString();
  }

  enmascaraTarjeta() {
    let tarjeta = this.number;
    this.secretNumber = tarjeta.replace(/.(?=.{4})/g, '*');
    this.encriptaTarjeta();
  }

  enviar() {
    this.clave = this.number[0] + this.number[1];
    this.enmascaraTarjeta();
    let tarjeta: Tarjeta = {
      name: this.name,
      number: this.number,
      cvc: this.cvc,
      expiry: this.expiry,
      clave: this.clave,
      secretNumber: this.secretNumber,
    }
    console.log(tarjeta);
    this.confirmarGuardado();
  }

  confirmarGuardado() {
    // funcion que despliega una alert con icono de check que indique que se guardo la tarjeta

    this.presentSuccessAlert('Tarjeta guardada con éxito');

  }

  async presentSuccessAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Éxito!',
      subHeader: msg,
      buttons: [{
        text: 'Aceptar',
        role: 'accept',
        handler: () => {
          this.router.navigate(['/mis-autos']);
        }
      }],
    });
    await alert.present();
  }
}
