import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { User } from 'src/app/interfaces';
import { LogoutService } from 'src/app/services/logout.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'perfil.page.html',
  styleUrls: ['perfil.page.scss']
})

export class PerfilPage {
  isActionSheetOpen = false;
  confirmLogOut() {
    console.log('Confirm Log Out');
    this.presentActionSheet();
  }
  public actionSheetButtons = [
    {
      text: 'Cerrar sesión',
      role: 'destructive',
      icon: 'log-out-outline',
      handler: () => {
        this.logout();
      }
    },
    {
      text: 'Cancelar',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }
  ]

  async presentActionSheet() {
    this.isActionSheetOpen = true;
    const actionSheet = await this.actionSheetController.create({
      header: 'Confirma que deseas cerrar sesión',
      buttons: this.actionSheetButtons,
      mode: 'ios'
    });
    await actionSheet.present();
  }


  usuario: User = {
    email: '',
    name: '',
    photo: '',
    fiscal_data: {
      RFC: '',
      domicilioFiscal: '',
      razonSocial:'' ,
      regimenFiscal:'' ,
      usoCFDI:'' ,
    }
  }
  personImage = 'assets/img/person.png';

  constructor(private logoutService: LogoutService, private actionSheetController: ActionSheetController, private router : Router) {
    this.loadUserData();
  }

  loadUserData(){
    let userData = localStorage.getItem('user-data');
    if(userData){
      console.log("user-data: ",userData);
      this.usuario = JSON.parse(userData);
      this.usuario.photo = this.usuario.photo ? this.usuario.photo : this.personImage;
      console.log("usuario: ",this.usuario);
    }
  }

  logout(){
    console.log(this.logoutService.getLogout());
  }
  editarPerfil(){
    console.log('Editar perfil');
  }
  setOpen(isOpen: boolean) {
    this.isActionSheetOpen = isOpen;
  }

  miCuenta = [
    {
      title: 'Configuración de la cuenta',
      icono: 'cog-outline',
    },
    {
      title: 'Métodos de pago',
      icono: 'wallet-outline',
      onclick: () => {
        console.log('Metodos de pago');
        this.router.navigate(['/tarjetas']);
      }
    },
    {
      title: 'Facturación',
      icono: 'documents-outline',
      onclick: () => {
        console.log('Facturación');
        this.router.navigate(['/facturacion']);
      }
    },
    {
      title: 'Mis autos',
      icono: 'car-sport-outline',
      onclick: () => {
        console.log('Mis autos');
        this.router.navigate(['/mis-autos']);
      }
    }
  ]

  sobreUful = [
    {
      title: 'Sobre el servicio',
      icono: 'help-circle-outline',
    },
    {
      title: 'Blog',
      icono: 'book-outline',
    },
    {
      title: 'Ayuda',
      icono: 'help-buoy-outline',
    }
  ]

  legal = [
    {
      title: 'Términos y condiciones',
      icono: 'document-outline',
    },
    {
      title: 'Aviso de privacidad',
      icono: 'document-text-outline',
    },
  ]


}
