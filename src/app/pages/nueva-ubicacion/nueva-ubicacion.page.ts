import { Component, ElementRef, Inject, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { GooglemapsService } from 'src/app/services/googlemaps.service';
import { google } from 'google-maps';
import { UbicacionService } from 'src/app/services/ubicacion.service';
import { Geolocation } from '@capacitor/geolocation';

@Component({
  selector: 'app-nueva-ubicacion',
  templateUrl: './nueva-ubicacion.page.html',
  styleUrls: ['./nueva-ubicacion.page.scss'],
})
export class NuevaUbicacionPage {

  constructor(private router: Router,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any,
    private googlemapsService: GooglemapsService,
    private ubicacion: UbicacionService) {}

  showButton: boolean = false;

  confirmar() {
    console.log('confirmar nueva ubicacion:' , this.selectedValue);
    this.ubicacion.direccion.calle = this.selectedValue.address_components[1]?.long_name || '';
    this.ubicacion.direccion.numero = this.selectedValue.address_components[0]?.long_name || '';
    this.ubicacion.direccion.ubicacion.lat = this.selectedValue.geometry.location.lat();
    this.ubicacion.direccion.ubicacion.lng = this.selectedValue.geometry.location.lng();
    this.ubicacion.direccion.colonia = this.selectedValue.address_components[2]?.long_name || '';
    this.ubicacion.direccion.ciudad = this.selectedValue.address_components[3]?.long_name || '';
    this.ubicacion.direccion.estado = this.selectedValue.address_components[4]?.long_name || '';
    this.ubicacion.direccion.codigoPostal = this.selectedValue.address_components[6]?.long_name || '' ;
    this.router.navigate(['/tabs/carga']);
  }

  selectResult(result: google.maps.GeocoderResult) {
    this.selectedValue = result;
    this.showButton = true;
    this.selectedValueString = this.results[0].formatted_address;
        const position = {
          lat: result.geometry.location.lat(),
          lng: result.geometry.location.lng(),
        }
        this.addMarker(position);
        this.map.setCenter(position);
        this.showResults = false;
    }
  results: google.maps.GeocoderResult[] = [];
  selectedValue : google.maps.GeocoderResult = {} as google.maps.GeocoderResult;
  selectedValueString: string = '';
  showResults: boolean = false;




  @ViewChild('mapElement') divMap: ElementRef | undefined;
  @Input() position = {
    lat: 19.045958,
    lng: -98.207588
  }

  map: any;
  userMarker: any;
  positionSet: any;

  ngOnInit() {
    this.init();
  }

  async init() {
    this.googlemapsService.init(this.renderer, this.document).then(() => {
      this.initMap();
    }).catch((err) => {
      console.log(err);
    });
  }

  initMap() {
    const position = this.position;
    let latLng = new google.maps.LatLng(position.lat, position.lng);

    let mapOptions: google.maps.MapOptions = {
      center: latLng,
      zoom: 14,
      disableDefaultUI: true,
      clickableIcons: false,
      styles: [
        {
          featureType: "poi",
          stylers: [{ visibility: "off" }],
        },
      ]
    }
    this.map = new google.maps.Map(this.divMap?.nativeElement, mapOptions);
    this.userMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable: true,
    });
    this.clickHandleEvent();
    this.addMarker(position);

  }

  clickHandleEvent(){
    this.showButton = true;
    this.map.addListener('click', (event: any) => {
      console.log(event);
      const position = {
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
      }
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({ location: position }, (results: google.maps.GeocoderResult[], status: any) => {
        if(status === google.maps.GeocoderStatus.OK){
          this.selectedValue = results[0];
          this.selectedValueString = results[0].formatted_address;
          console.log("selectedValue: ",this.selectedValue);
        }else{
          this.results = [];
        }
      });
      this.addMarker(position);
    });
  }

  addMarker(position: any){
    let latLng = new google.maps.LatLng(position.lat, position.lng);
    this.userMarker.setPosition(latLng);
    this.map.panTo(latLng);
    this.map.setZoom(14);
    this.positionSet = position;
  }

  handleInput($event: any) {
    this.showButton = false;
    const query = $event.target.value;
    console.log("query: ",query);
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: query, componentRestrictions: { country: 'MX' } }, (results: google.maps.GeocoderResult[], status: any) => {
      if(status === google.maps.GeocoderStatus.OK){
        this.results = results
        this.showResults = true
      }else{
        this.results = [];
      }
    });
    console.log("res: ",this.results);

  }

  async goToMyLocation(): Promise<[number, number]> {
    console.log("Obteniendo ubicación actual");
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        ({coords}) => {
          console.log("Geolocalizacion obtenida: ", coords);
          const useLocation = {lat:coords.latitude, lng:coords.longitude};
          console.log("useLocation: ", useLocation);
          this.addMarker(useLocation);
          const geocoder = new google.maps.Geocoder();
          geocoder.geocode({ location: useLocation, componentRestrictions: { country: 'MX' } }, (results: google.maps.GeocoderResult[], status: any) => {
            if(status === google.maps.GeocoderStatus.OK){
              this.results = results
              this.selectedValueString = results[0].formatted_address;
              this.selectedValue = results[0];
            }else{
              this.results = [];
            }
          });
          resolve([coords.latitude, coords.longitude]);
        },
        (err) => {
          console.log("Error al obtener ubicación: ", err);
          reject(err);
        })})
  };



  // selectedValue: Direccion = {
  //   ubicacion: { lat: 19.045958, lng: -98.207588},
  //   calle: '',
  //   numero: '',
  //   colonia: '',
  //   ciudad: '',
  //   estado: '',
  //   codigoPostal: '',
  // };

  // selectedValueString: string = '';


  // public municipios: Municipio[]  = [
  //   { nombre: "Atlixco", ubicacion: { "lat": 18.9121, "lng": -98.4385 } },
  //   { nombre: "Puebla", ubicacion: { "lat": 19.0414, "lng": -98.2063 } },
  //   { nombre: "Morelia", ubicacion: { "lat": 19.7033, "lng": -101.1923 } },
  //   { nombre: "Tehuacán", ubicacion: { "lat": 18.4619, "lng": -97.3926 } },
  //   { nombre: "Amozoc", ubicacion: { "lat": 19.0511, "lng": -98.0503 } },
  //   { nombre: "San Andrés Cholula", ubicacion : { "lat": 19.0415, "lng": -98.3022 } },
  //   { nombre: "San Martín Texmelucan", ubicacion: { "lat": 19.2768, "lng": -98.4381 } },
  //   { nombre: "Cholula de Rivadavia", ubicacion: { "lat": 19.0641, "lng": -98.3035 } }
  // ];



}
