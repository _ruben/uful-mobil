import { Component, OnInit } from '@angular/core';

import { Auto, Pedido } from '../../interfaces/index';
import { ActivatedRoute, Router } from '@angular/router';
import { TicketService } from 'src/app/services/ticket.service';
import { MetodosDePagoService } from 'src/app/services/metodos-de-pago.service';
import { EventService } from 'src/app/services/event.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertController, ModalController } from '@ionic/angular';
import { SeleccionAutoComponent } from 'src/app/components/seleccion-auto/seleccion-auto.component';
import { AutosService } from '../../services/autos.service';
import { PreciosService } from 'src/app/services/precios.service';
@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.page.html',
  styleUrls: ['./resumen.page.scss'],
})



export class ResumenPage implements OnInit {

  nuevaTarjeta() {
    this.router.navigate(['/nueva-tarjeta']);
  }

  realizarPedido() {
    this.loadingService.showLoading();
    localStorage.setItem('cargas', JSON.stringify(this.pedido));
    this.ticket.createTicket(this.pedido);
  }

  metodoDePago = {
    nombreTitular: "",
    icon: "",
    terminacion: "",

  }
  pedido : Pedido = {
    estacion: {
      nombre: "",
      logo: "",
      direccion: {
        ubicacion: { lat: 0, lng: 0 },
        calle: "",
        numero: "",
        colonia: "",
        ciudad: "",
        estado: "",
        codigoPostal: "",
      },
      calificaciones: [],
      tiempo: "",
      distancia: "",
      servicios: [],
    },
    carga: {
      status: "Confirmando carga",
      auto : {
        car_model: '', license_plates: '', fuel: '',
        year: '',
        nickname: '',
        state: '',
        brand: '',
        favorite: false
      },
      fecha: "",
      hora: "",
      total: "",
      litros: "",
      tipoDeServicio: true,
    }
  }


  constructor( private ticket: TicketService, private route: ActivatedRoute, private router: Router, private metodos : MetodosDePagoService, private loadingService: LoadingService, private modalCtl : ModalController, private eventService: EventService, private autosService: AutosService, private alertCtrl: AlertController, private precios : PreciosService, private loading: LoadingService) {
    this.metodoDePago = this.metodos.getDefault();
  }

  async presentSuccessAlert(msg: string, auto: Auto) {
    const alert = await this.alertCtrl.create({
      header: 'Alerta!',
      subHeader: msg,
      buttons: [{
        text: 'Aceptar',
        role: 'accept',
        handler: () => {
          this.actualizaPrecio(auto.fuel);
          this.pedido.carga.auto = auto;

        }
      },
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ],
    });
    await alert.present();
  }

  actualizaPrecio(fuel: string){
    switch(fuel){
      case 'Regular': this.pedido.carga.total = (parseInt(this.pedido.carga.litros) * this.precios.getPrecioMagna()).toFixed(2).toString() ; break;
      case 'Premium': this.pedido.carga.total = (parseInt(this.pedido.carga.litros) * this.precios.getPrecioPremium()).toFixed(2).toString() ; break;
      case 'Diesel': this.pedido.carga.total = (parseInt(this.pedido.carga.litros) * this.precios.getPrecioDiesel()).toFixed(2).toString() ; break;
    }
    this.pedido.carga.auto.fuel = fuel;
  }

  async updateAutos(){
    this.loading.showLoading()
    this.autosService.getAutos().subscribe((data) => {
      this.loading.hideLoading();
      this.openCarModal();
    });
  }

  async openCarModal() {
    const modal = await this.modalCtl.create({
      component: SeleccionAutoComponent,
      componentProps: {
        autos: this.autosService.autosObtenidos,
      },
      breakpoints: [0, .8, 1],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });

    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        if(result.data.data.fuel !== this.pedido.carga.auto.fuel){
          this.presentSuccessAlert(
            'Los tipos de combustible son distintos, se conservarán los litros indicados, pero el total se verá modificado',
            result.data.data);
        }
        else{
          this.pedido.carga.auto = result.data.data;
        }
        this.pedido.carga.auto = this.pedido.carga.auto;
      }
    });

    return await modal.present();
  }


  ngOnInit() {


    if(this.route.snapshot.data['special']){
      const pedidoString = this.route.snapshot.data['special'];
      this.pedido = JSON.parse(pedidoString);
    }

  }

  changeTipoServicio($event: any) {
    ($event.detail.checked) ? this.pedido.carga.tipoDeServicio = true : false;
  }

}
