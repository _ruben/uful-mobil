import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevoAutoPageRoutingModule } from './nuevo-auto-routing.module';

import { NuevoAutoPage } from './nuevo-auto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevoAutoPageRoutingModule
  ],
  declarations: [NuevoAutoPage]
})
export class NuevoAutoPageModule {}
