import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Auto, ModelosPorMarca } from 'src/app/interfaces';
import { AutosService } from 'src/app/services/autos.service';
import { ModalController } from '@ionic/angular';
import { SeleccionMarcaComponent } from 'src/app/components/seleccion-marca/seleccion-marca.component';
import { SeleccionModeloComponent } from 'src/app/components/seleccion-modelo/seleccion-modelo.component';
import { SeleccionColorComponent } from 'src/app/components/seleccion-color/seleccion-color.component';
import { SeleccionEstadoComponent } from 'src/app/components/seleccion-estado/seleccion-estado.component';


@Component({
  selector: 'app-nuevo-auto',
  templateUrl: './nuevo-auto.page.html',
  styleUrls: ['./nuevo-auto.page.scss'],
})
export class NuevoAutoPage implements OnInit {
  setFav() {
    this.auto.favorite = !this.auto.favorite;
  }

  constructor( private autoServ: AutosService,
    private alertController: AlertController,
    public modalCtrl : ModalController) {  }

  years : string [] = ["2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021","2022", "2023"]

  auto : Auto = {
    car_model: '',
    license_plates: '',
    fuel: '',
    color: '#000000',
    year: '',
    nickname: '',
    state: '',
    brand: '',
    favorite: false
  }

  async presentErrorAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Espera!',
      subHeader: msg,
      buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
    });
    await alert.present();
  }

  async openModalMarca() {
    const modal = await this.modalCtrl.create({
      component: SeleccionMarcaComponent,
      breakpoints: [0, .8],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });

    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        const selectedOption = result.data;
        console.log('Opción seleccionada:', selectedOption.data);
        this.auto.brand = selectedOption.data;
        this.filterModels();
      }
    });

    return await modal.present();
  }
  marcas: ModelosPorMarca = {
    Nissan: ['Centra', 'Sentra', 'Versa'],
    Ford: ['Mustang', 'Fiesta', 'Focus'],
    Chevrolet: ['Camaro', 'Cruze', 'Spark'],
  };
  modelosFiltrados: string[] = this.marcas[this.auto.brand];

  async openModalModelo() {
    console.log("openModalModelo con marca: ", this.auto.brand, " y modelos: ", this.modelosFiltrados);
    const modal = await this.modalCtrl.create({
      component: SeleccionModeloComponent,
      componentProps: {
        modelos: this.modelosFiltrados
      },
      breakpoints: [0, .8],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });
    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        const selectedOption = result.data;
        console.log('Opción seleccionada:', selectedOption.data);
        this.auto.car_model = selectedOption.data;
      }
    });
    return await modal.present();
  }


  filterModels() {
    this.auto.car_model = ''
    this.modelosFiltrados = this.marcas[this.auto.brand];
  }

  colorTxt = 'Negro'
  async openModalColor() {
    const modal = await this.modalCtrl.create({
      component: SeleccionColorComponent,
      breakpoints: [0, .8],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });
    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        const selectedOption = result.data;
        console.log('Opción seleccionada:', selectedOption.data);
        this.auto.color = selectedOption.data.value;
        this.colorTxt = selectedOption.data.name;
      }
    });
    return await modal.present();
  }

  async openModalEstado(){
    const modal = await this.modalCtrl.create({
      component: SeleccionEstadoComponent,
      breakpoints: [0, .8, 1],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });
    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        const selectedOption = result.data;
        console.log('Opción seleccionada:', selectedOption.data);
        this.auto.state = selectedOption.data;
      }
    });
    return await modal.present();
  }

  async guardar() {
    if(this.auto.brand == '') {
      //|| this.auto.model == '' || this.auto.year == '' || this.auto.placa == '' || this.auto.tipo == '' || this.auto.nickname == '' || this.auto.state == ''
      this.presentErrorAlert("Selecciona la marca del auto");
      console.log("Selecciona una marca");
      return;
    }
    else if(this.auto.car_model == '') {
      this.presentErrorAlert("Selecciona el modelo del auto");
      console.log("Selecciona un modelo");
      return;
    }
    else if(this.auto.year == '') { //es obligatorio?!
      this.presentErrorAlert("Ingresa el año del auto");
      console.log("Selecciona año");
      return;
    }
    else if(this.auto.color == ''){
      this.presentErrorAlert("Selecciona el color del auto");
      console.log("Selecciona un color");
      return;
    }
    else if(this.auto.license_plates == '') {
      this.presentErrorAlert("Ingresa la placa del auto");
      console.log("Selecciona una placa");
      return;
    }
    else if(this.auto.fuel == '') {
      this.presentErrorAlert("Selecciona el tipo de combustible del auto");
      console.log("Selecciona un tipo");
      return;
    }
    else if(this.auto.state == '') {
      this.presentErrorAlert("Selecciona el estado del auto");
      console.log("Selecciona un estado");
      return;
    }else{
      (this.auto.nickname == '') ? this.auto.nickname = this.auto.brand + " " + this.auto.car_model : this.auto.nickname = this.auto.nickname;
      console.log("Guardando auto: ", this.auto);
      const resp = this.autoServ.postAuto(this.auto);
      console.log("Respuesta: ", resp);
    }

  }


  ngOnInit() {
  }

}
