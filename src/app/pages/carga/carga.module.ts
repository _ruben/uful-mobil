import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CargaPage } from './carga.page';


import { CargaPageRoutingModule } from './carga-routing.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    CargaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CargaPage]
})
export class CargaPageModule {}
