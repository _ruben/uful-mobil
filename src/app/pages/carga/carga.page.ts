import { Component, ChangeDetectorRef } from '@angular/core';

import { IonAccordionGroup, ModalController } from '@ionic/angular';
import { Auto, Estacion, Pedido, TicketDeCarga, Ubicacion } from 'src/app/interfaces';
import { Carga, Direccion } from '../../interfaces/index';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

import { UbicacionService } from 'src/app/services/ubicacion.service';
import { AutosService } from 'src/app/services/autos.service';
import { TicketService } from 'src/app/services/ticket.service';
import { QrService } from 'src/app/services/qr.service';
import { GooglemapsService } from 'src/app/services/googlemaps.service';
import { google } from 'google-maps';
import { PedidoService } from 'src/app/services/pedido.service';
import { EventService } from 'src/app/services/event.service';
import { Subscription } from 'rxjs';
import { LoadingService } from 'src/app/services/loading.service';
import { SeleccionDireccionComponent } from 'src/app/components/seleccion-direccion/seleccion-direccion.component';
import { SeleccionAutoComponent } from 'src/app/components/seleccion-auto/seleccion-auto.component';
import { RefreshHomeService } from 'src/app/services/refresh-home.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'carga.page.html',
  styleUrls: ['carga.page.scss'],
})
export class CargaPage {
ticket() {
  this.imgQR = ""
}



  selectedValue : google.maps.GeocoderResult = {} as google.maps.GeocoderResult;

  tipoFuel: string = "Regular"; //por default

  cargaTicket: TicketDeCarga = {
    _id: '',
    ticket_folio: null,
    liters: 0,
    liters_loaded: 0,
    exact_charge: null,
    price_per_liter: 0,
    total: 0,
    total_charged: 0,
    car_id: '',
    status: '',
    customer_id: '',
    ticket_number: '',
    created_date: undefined,
    created_time: '',
    created_timestamp: 0,
    createdAt: undefined,
    updatedAt: undefined,
    fuel: {
      _id: '',
      name: '',
      type: '',
    }
  }; //este objeto sirve para cachar el ticket desde el servicio

  autosRegistrados: Auto[] = []; //almacena los autos registrados del usuario que vienen del servicio

  pedido : Pedido = { //objeto esqueleto para el pedido
    estacion: {
      nombre: '',
      logo: '',
      direccion: {
        ubicacion: { lat: 0, lng: 0 },
        calle: '',
        numero: '',
        colonia: '',
        ciudad: '',
        estado: '',
        codigoPostal: '',
      },
      calificaciones: [],
      tiempo: '',
      distancia: '',
      servicios: [],
    },
    carga: {
      status: "",
      auto: {
        year: '',
        nickname: 'Selecciona un auto',
        color: '',
        car_model: '',
        license_plates: '',
        fuel: 'Regular',
        state: '',
        brand: '',
        favorite: false,
      },
      fecha: '',
      hora: '',
      litros: '20',
      total: '',
      tipoDeServicio: true,
    }
  };

  cargaNueva: Carga = this.pedido.carga; //carga nueva se usa cuando se genera una nueva carga, se inicializa con los datos de la carga del pedido que son vacios por defecto
  cargaActiva : Carga | null = null ; //carga activa se usa cuando existe en local storage una carga activa

  estacion: Estacion | null = null; //estacion se usa para poder pasar el objeto completo en un Pedido cuando haya una carga activa, cuando no hay carga activa se usa la estacion por defecto




  direccion: Direccion = { //aca recibe la direccion actual
    ubicacion: { lat: 0, lng: 0 },
    calle: "",
    numero: "",
    colonia: '',
    ciudad: 'Puebla',
    estado: '',
    codigoPostal: ''
  }
  getPlaceholder() {
    return (this.direccion.calle !== '')? this.direccion.calle : '' + ' ' + (this.direccion.numero !== '')? this.direccion.numero : '' + ' ' + (this.direccion.colonia !== '')? this.direccion.colonia : '' + ' ' + (this.direccion.ciudad !== '')? this.direccion.ciudad : '' + ' ' + (this.direccion.estado !== '')? this.direccion.estado : '' + ' ' + (this.direccion.codigoPostal !== '')? this.direccion.codigoPostal : '';
  }

  updateSubscription: Subscription = new Subscription; //se usa para actualizar la pagina cuando se selecciona un auto

  accordionGroupChange( event: any) { //maneja el cambio de valor del accordion de autos

    if(event.detail.value == 'Premium' || event.detail.value == 'Diesel' || event.detail.value == 'Regular'){
      this.cargaNueva.auto.fuel = event.detail.value;
      console.log(this.cargaNueva.auto.fuel);
    }
  }



  async showModalUbicaciones() {
    const modal = await this.modalCtrl.create({
      mode: 'ios',
      component: SeleccionDireccionComponent,
      breakpoints: [0, .8],
      initialBreakpoint: .8,
      handle: false,
      componentProps: {
        pedido: this.pedido //manda la data que se recibe desde Input en EstacionSheetModalComponent
      },
    });
    modal.onDidDismiss().then((data) => {
      console.log(data);
      if(data.data.role === 'selected'){
        if(data.data.data === 'actual'){
          this.goToMyLocation();
        }else if(data.data.data === 'mapa'){
          this.router.navigateByUrl('/nueva-ubicacion');
        }
      }
    });

    await modal.present();
  }

  goToMyLocation() { //esta funcion obtiene la ubicacion actual del usuario
    console.log("obteniendo ubicacion actual");
    try{
      this.ubicacion.getUserLocation().then((position) => {
        console.log("ubicacion obtenida en carga ",position);
        this.direccion = this.ubicacion.getDireccion();
      }).catch((error) => {
        console.log(error);
      });
    }catch(error) {
      console.log(error);
    }
  }

  iniciarPedido($event: any) { //iniciar pedido es cuando se da en el boton "CARGAR" en la pantalla de carga
    console.log($event);
    this.cargaNueva.fecha = this.obtenerFecha();
    this.cargaNueva.hora = this.obtenerHora();
    console.log("Iniciando carga: ", this.cargaNueva);
    this.dataService.setData(10, this.cargaToJson(this.cargaNueva));
    this.router.navigateByUrl('/ingresa-carga/10');

  } //se asigna la fecha y hora de la carga nueva por que este boton solo aparece cuando no hay una carga activa


  imgQR = ''


constructor(private router : Router,
  private dataService: DataService,
  private ubicacion: UbicacionService,
  private autos : AutosService, private ticketService : TicketService,
  private changeDetectorRef : ChangeDetectorRef, private googlemaps : GooglemapsService,
  private eventoService: EventService, private loadingService: LoadingService,
  private modalCtrl: ModalController,
  private refresh : RefreshHomeService) {
    //this.cargarServicios();
    this.updateSubscription = this.eventoService.update$.subscribe((data) => {
      // Realiza la lógica de actualización en la página 1 con los datos recibidos
      console.log('Actualización recibida en la página 1:', data);
      if(data){
        console.log("actualizando pagina ", data);
      }
      // Realiza las acciones necesarias para actualizar la página 1
    });
  }

  cargarServicios(){
      this.loadingService.showLoading('Obteniendo los mejores precios').then(() => {
        this.obtenerAutos();
        this.direccion = this.ubicacion.getDireccion();
      });
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
  }

  obtenerAutos(){
    try{
      console.log("obteniendo autos -----");
      this.autos.getAutos().subscribe(response => {
        console.log(response);
        this.autosRegistrados = response.cars;
        console.log("auto registrados: .... .. . . . ",this.autosRegistrados);
        if(this.autosRegistrados.length !== 0) {
          const favoriteAutoIndex = this.autosRegistrados.findIndex(auto => auto.favorite);
          if (favoriteAutoIndex !== -1 ) this.pedido.carga.auto = this.autosRegistrados[favoriteAutoIndex];
        }
        this.loadingService.hideLoading();
      }, error => {
        console.log("@subscription",error);
      });
    }catch(error){
      console.log("@catchSub",error);
    }
  }


  varColor = "#ffffff'"
  async openCarModal() {
    const modal = await this.modalCtrl.create({
      component: SeleccionAutoComponent,
      componentProps: {
        autos: this.autosRegistrados,
      },
      breakpoints: [0, .8, 1],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });

    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        this.pedido.carga.auto = result.data.data;
        this.varColor = this.pedido.carga.auto.color || "#ffffff";
        this.changeDetectorRef.detectChanges();
        console.log("auto seleccionado: ",this.pedido.carga.auto);
      }
      if(result.role === 'backdrop'){
        console.log("backdrop");
      }
    });

    return await modal.present();
  }

  // async ngOnInit() {
  //   this.cargarServicios();
  // }

  async ionViewWillEnter(){
    this.refresh.refresh$.subscribe(() => {
      console.log("llego actualizacion desde ticket service a carga")
      let qr = localStorage.getItem('qr')
      if(qr != null){
        console.log("Hay ticket cargado");
        this.imgQR = qr;
        const autoconticketID = localStorage.getItem('autoID')
        this.ticketService.getTicket( autoconticketID || '').subscribe(
          (data) => {
            console.log("Ticket obtenido: ", data);
            // Manejar la respuesta aquí
            const lastTicketIndex = data.data.length - 1;
            this.cargaTicket = data.data[lastTicketIndex];
          },
          (error) => {
            console.log(`Error al obtener ticket. ${error.error.message}`);
            console.log(error);
          }
        ) //obtiene la info de ticket
        console.log("carga ticket ", this.cargaTicket);
        this.cargaActiva = this.pedido.carga
        this.estacion = this.pedido.estacion
        console.log("carga activa en did: ",this.cargaActiva);
      }else{
        console.log("Did Enter: No hay cargas activas");
        this.cargaActiva = null;
      }
    });
    let qr = localStorage.getItem('qr')
      if(qr != null){
        console.log("Hay ticket cargado");
        this.imgQR = qr;
        const autoconticketID = localStorage.getItem('autoID')
        this.ticketService.getTicket( autoconticketID || '').subscribe(
          (data) => {
            console.log("Ticket obtenido: ", data);
            // Manejar la respuesta aquí
            const lastTicketIndex = data.data.length - 1;
            this.cargaTicket = data.data[lastTicketIndex];
          },
          (error) => {
            console.log(`Error al obtener ticket. ${error.error.message}`);
            console.log(error);
          }
        ) //obtiene la info de ticket
        console.log("carga ticket ", this.cargaTicket);
        this.cargaActiva = this.pedido.carga
        this.estacion = this.pedido.estacion
        console.log("carga activa en did: ",this.cargaActiva);
      }else{
        console.log("Did Enter: No hay cargas activas");
        this.cargaActiva = null;
      }
    this.cargarServicios();
    this.direccion = this.ubicacion.getDireccion();
  }

  nuevoAuto() { //funcion que se ejecuta cuando se da click en el boton de nuevo auto, este solo aparece cuando no hay autos registrados por el usuario
    this.router.navigate(['/nuevo-auto']);
  }
  cargaToJson(carga: Carga) {
    return JSON.stringify(carga);
  } //pasar una carga a json

  private obtenerFecha(){
    let today = new Date();
    let todayString = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    return todayString;
  }
  private obtenerHora(){
    let today = new Date();
    let todayString = today.getHours() + ":" + today.getMinutes();
    return todayString;
  }

}
