import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController } from '@ionic/angular';

import { Pedido } from '../../interfaces/index';

@Component({
  selector: 'app-tab2',
  templateUrl: 'actividad.page.html',
  styleUrls: ['actividad.page.scss'],
})
export class ActividadPage {
  cargasAnteriores: boolean = false;
  pedidosAnterioresList: Pedido[] = [
    {
      carga: {
        status: "Completado",
        auto : {
          brand: "Suzuki",
          car_model: "Swift",
          year: "2019",
          license_plates: "ABC-123",
          fuel: "Regular",
          nickname: "Mi Swift",
          state: "Nuevo León",
          favorite: false
        },
      fecha: "2023-4-3",
      hora: "10:00",
      total: "500",
      litros: "15.13",
    },
    estacion: {
      nombre: "Estación 1",
      logo: "../../../assets/icon/estacion1.svg",
      direccion: {
        ubicacion: { lat: 0, lng: 0 },
        calle: "Calle 1",
        numero: "123",
        colonia: "Colonia 1",
        ciudad: "Ciudad 1",
        estado: "Estado 1",
        codigoPostal: "12345",
      },
      calificaciones: [],
      tiempo: "10 min",
      distancia: "1.5 km",
      servicios: [""],
    }
  },
  {
    carga: {
      status: "Completado",
        auto: { brand: "Susuki", car_model : "Swift",
        license_plates: "DEF-456",
        fuel: "Diesel",
        year: "2018",
        nickname: "Mi Swift",
        state: "Nuevo León",
        favorite: false
      },
      fecha: "2023-3-04",
      hora: "14:20",
      total: "200",
      litros: "10.13",
    },estacion: {
        nombre: "Estación 2",
        logo: "../../../assets/icon/estacion2.svg",
        direccion: {
          ubicacion: { lat: 0, lng: 0 },
          calle: "Calle 2",
          numero: "456",
          colonia: "Colonia 2",
          ciudad: "Ciudad 2",
          estado: "Estado 2",
          codigoPostal: "67890",
        },
        calificaciones: [],
        tiempo: "20 min",
        distancia: "2.5 km",
        servicios: [""],
      }},
  ];

  async presentAlert(msg: string = "Carga eliminada") {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: ['OK'],
    });

    await alert.present();
  }

  autoAscendente: boolean = false;
  fechaDescendente: boolean = true;

  ordenarProductoPorFechaMasReciente(): Pedido[] {
    return this.pedidosAnterioresList.sort((a, b) => {
      return new Date(b.carga.fecha).getTime() - new Date(a.carga.fecha).getTime();
    });
  }

  ordenarPorFecha() {
    if (this.fechaDescendente) {
      this.ordenarProductoPorFechaMasReciente();
    } else {
      this.ordenarProductoPorFechaMasReciente().reverse();
    }
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [
        {
          text: 'Eliminar activas',
          role: 'destructive',
          handler: () => {
            if(this.cargaActiva !== null){
              localStorage.removeItem('qr');
              localStorage.removeItem('autoID');
              localStorage.removeItem('cargas');
              this.pedidosAnterioresList.push(this.cargaActiva);
              this.cargaActiva = null;
              this.presentAlert();
            }else{
              this.presentAlert("No hay cargas activas");
            }
          }
        },
        {
          text: 'Ordenear por auto',
          handler: () => {
            console.log('Order by auto seleccionada', this.autoAscendente);
            this.autoAscendente = !this.autoAscendente;
            this.ordenarPorFecha();
          }
        },
        {
          text: 'Ordenear por fecha',
          handler: () => {
            console.log('Order by fecha seleccionada', this.fechaDescendente);
            this.fechaDescendente = !this.fechaDescendente;
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  cargaActiva: Pedido | null = null;


  constructor(public actionSheetController: ActionSheetController, private alertController: AlertController) { }
  ngOnInit() {
    console.log("ngOnInitCarga");
    this.ordenarPorFecha();
    const cargasString = localStorage.getItem('cargas')
    if( cargasString != "" && cargasString != null){
      this.cargaActiva = JSON.parse(cargasString);
      console.log("carga activa: ",this.cargaActiva);
    }else{
      console.log("No hay cargas activas");
      this.cargaActiva = null;
    }
  }

  ionViewDidEnter() {
    console.log("ngOnInitCarga");
    this.ordenarPorFecha();
    const cargasString = localStorage.getItem('cargas')
    if( cargasString != "" && cargasString != null){
      this.cargaActiva = JSON.parse(cargasString);
      console.log("carga activa: ",this.cargaActiva);
    }else{
      console.log("No hay cargas activas");
      this.cargaActiva = null;
    }

  }


}

