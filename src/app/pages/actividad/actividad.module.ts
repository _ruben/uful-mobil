import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActividadPage } from './actividad.page';


import { ActividadPageRoutingModule } from './actividad-routing.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ActividadPage]
})
export class ActividadPageModule {}
