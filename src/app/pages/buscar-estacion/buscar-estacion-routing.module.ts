import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuscarEstacionPage } from './buscar-estacion.page';

const routes: Routes = [
  {
    path: '',
    component: BuscarEstacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuscarEstacionPageRoutingModule {}
