import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { IonicModule } from '@ionic/angular';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { BuscarEstacionPageRoutingModule } from './buscar-estacion-routing.module';
import { BuscarEstacionPage } from './buscar-estacion.page';
import { ComponentsModule } from '../../components/components.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuscarEstacionPageRoutingModule,
    ComponentsModule,
    MatBottomSheetModule,
    ScrollingModule,
  ],
  declarations: [BuscarEstacionPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BuscarEstacionPageModule {}
