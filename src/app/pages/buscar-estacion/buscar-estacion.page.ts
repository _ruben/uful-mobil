import { Component, ElementRef,Inject, Input, OnInit, Renderer2 , ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Estacion, Ubicacion } from 'src/app/interfaces';
import { Carga } from '../../interfaces/index';
import { DOCUMENT } from '@angular/common';
import { google } from 'google-maps';
import { GooglemapsService } from 'src/app/services/googlemaps.service';
import { EstacionesService } from 'src/app/services/estaciones.service';
import { DataService } from 'src/app/services/data.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-buscar-estacion',
  templateUrl: './buscar-estacion.page.html',
  styleUrls: ['./buscar-estacion.page.scss'],
})
export class BuscarEstacionPage {

  @ViewChild('mapElement') divMap: ElementRef | undefined;

  carga : Carga = {
    status: 'En busca de estacion',
    auto : {
      car_model: '', license_plates: '', fuel: '',
      year: '',
      nickname: '',
      state: '',
      brand: '',
      favorite: false
    },
    fecha: '',
    hora: '',
    total: '',
    litros: '',
    tipoDeServicio: true,
  }
  estacion : Estacion = {
    nombre: 'Nombre de estacion',
    logo: '../../../assets/img/supercarga-logo.svg',
    direccion: {
      ubicacion: { lat: 0, lng: 0 },
      calle: 'Calle 1',
      numero: '1',
      ciudad: 'Ciudad 1',
      colonia: 'colonia 1',
      estado: 'estado 1',
      codigoPostal: 'codigo postal 1'
    },
    calificaciones: [{
      nombre:"servicio",
      valoracion:0
    },
    {
      nombre: "atencion",
      valoracion:0
    },
    {
      nombre: "limpieza",
      valoracion:0
    }],
    tiempo: 'min',
    distancia: 'km',
    servicios: ['medkit', 'wifi', 'car-electric', 'car-wash', 'car-parking', 'storefront', 'restaurant', 'cafe', 'bed']
  }
  estacionesObtenidas : Estacion[] = [];
  @Input() position = {
    lat: 19.0466335310171,
    lng: -98.20842362880778,
  }

  label ={
    titulo: 'Ud. está aquí',
    subtitulo : 'Seleccione una estacion en el mapa o en la lista'
  }

  map: any;
  userMarker: any;
  estacionesMarkers: any[] = [];
  infoWindow: any;
  positionSet : any;

  constructor(
    private navCtrl: NavController,
    private pedidoNuevo: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any,
    private googlemapsService: GooglemapsService,
    private estaciones: EstacionesService)
    {
      this.estacionesObtenidas = this.estaciones.estaciones.filter(estacion => estacion.direccion.ciudad == 'Puebla'); //obtiene las estaciones de la ciudad actual
    }

  ngOnInit() {
    this.init();
    if(this.route.snapshot.data['special']){
      console.log("getting carga from data @ ingresa-carga.page.ts")
      const cargaString = this.route.snapshot.data['special'];
      this.carga = JSON.parse(cargaString);
    }
  }

  async init(){
    this.googlemapsService.init(this.renderer, this.document).then( () => {
      this.initMap();
    }).catch((err: any) => {
      console.log(err);
    });
  }


  scrollToCard(index: number) {
    const cardElement = document.getElementById(`card-${index}`);
    if (cardElement) {
      cardElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }

  initMap() {
    const position = this.position;
    let latLng = new google.maps.LatLng(position.lat, position.lng);


let mapOptions: google.maps.MapOptions = {
  center: latLng,
  zoom: 14,
  disableDefaultUI: true,
  clickableIcons: false,
  styles: [
    {
      featureType: "poi",
      stylers: [{ visibility: "off" }],
    },
  ]
};

    this.map = new google.maps.Map(this.divMap?.nativeElement, mapOptions);
    this.userMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable: false,
    });
    this.creaMarcadoresDeEstaciones(this.estacionesObtenidas);
    this.infoWindow = new google.maps.InfoWindow();
    this.addMarker(this.position);
    this.setInfoWindow(this.userMarker,this.label);


  }

  creaMarcadoresDeEstaciones(estaciones: Estacion[]){
    estaciones.forEach((estacion, index) => {
      let latLng = new google.maps.LatLng(estacion.direccion.ubicacion.lat, estacion.direccion.ubicacion.lng);
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        draggable: false,
        icon: {
          url: estacion.logo,
          scaledSize: new google.maps.Size(50, 50),
        },
        position: latLng,
      });
      marker.addListener('click', () => {
        this.centraMapa(estacion.direccion.ubicacion);
        this.scrollToCard(index);
        //crea un infoWindow con el nombre de la estacion y un boton para seleccionar la estacion // por el uso de cards se vuelve redundante mostrar el infoWindow
        // let contentString =
        //     '<div id="content">'+
        //       '<div>'+
        //         '<h1>' + estacion.nombre + '</h1>'+
        //         '<button class="btn-seleccionar" id="seleccionarBtn">Seleccionar</button>'+
        //       '</div>'+
        //     '</div>';
        // this.infoWindow.setContent(contentString);
        // this.infoWindow.open(this.map, marker);
        // const seleccionarEstacion = (estacion: Estacion) => {
        //   console.log("seleccionando estacion desde pin");
        //   this.estacion = estacion;
        //   this.pedidoNuevo.setData(10, JSON.stringify({carga: this.carga, estacion: this.estacion}));
        //   this.router.navigate(['/resumen/10']);
        // };

        // const seleccionarBtn = document.getElementById('seleccionarBtn');
        // if(seleccionarBtn){

        //   console.log("seleccionando estacion desde pin");
        //   seleccionarBtn.addEventListener('click', () => seleccionarEstacion(estacion));
        // }
      });
      this.estacionesMarkers.push(marker);
    });
  }




  centraMapa(position: Ubicacion) {
    let latLng = {lat: position.lat, lng: position.lng};
    this.map.panTo(latLng);
    this.map.setZoom(14);
    this.positionSet = position;
  }

  addMarker(position: any){
    let latLng = new google.maps.LatLng(position.lat, position.lng);
    this.userMarker.setPosition(latLng);
    this.map.panTo(latLng);
    this.positionSet = position;
  }

  setInfoWindow(marker: any, label: any){

    const contentString =
      '<div id="content">'+
        '<div>'+
          '<h1>' + label.titulo + '</h1>'+
          '<p>' + label.subtitulo + '</p>'+
        '</div>'+
      '</div>';
    marker.addListener('click', () => {
      this.infoWindow.setContent(contentString);
      this.infoWindow.open(this.map, marker);
    });
  }


}
