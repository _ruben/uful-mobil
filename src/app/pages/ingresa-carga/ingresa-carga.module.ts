import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngresaCargaPageRoutingModule } from './ingresa-carga-routing.module';

import { IngresaCargaPage } from './ingresa-carga.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IngresaCargaPageRoutingModule
  ],
  declarations: [IngresaCargaPage]
})
export class IngresaCargaPageModule {}
