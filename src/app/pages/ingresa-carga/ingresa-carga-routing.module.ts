import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresaCargaPage } from './ingresa-carga.page';

const routes: Routes = [
  {
    path: '',
    component: IngresaCargaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngresaCargaPageRoutingModule {}
