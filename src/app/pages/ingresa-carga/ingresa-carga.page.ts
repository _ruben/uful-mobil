import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Carga } from '../../interfaces/index';
import { DataService } from 'src/app/services/data.service';
import { PreciosService } from 'src/app/services/precios.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-ingresa-carga',
  templateUrl: './ingresa-carga.page.html',
  styleUrls: ['./ingresa-carga.page.scss'],
})
export class IngresaCargaPage implements OnInit {

  onTotalChange(event: any) {
    const inputElement = event.target as HTMLInputElement;
    const totalValue = inputElement.value;
    console.log(totalValue);
    this.carga.litros = Math.floor(parseInt(totalValue) / this.precioUful).toString();
  }

  precioUful = 0;
  total = "0";
  carga: Carga = {
    status: "Iniciando carga",
    auto : {
      car_model: '', license_plates: '', fuel: '',
      year: '',
      nickname: '',
      state: '',
      brand: '',
      favorite: false
    },
    fecha: "",
    hora: "",
    total: "0",
    litros: "20",
    tipoDeServicio: true,
  };


  updateTotal(){
    this.total = `${this.carga.total}`;
  }

  updateLitrosOnTotalChange() {
    console.log("updateLitrosOnTotalChange")
    this.carga.total = (parseInt(this.carga.litros) * this.precioUful).toFixed(2).toString();
  }

  cargaToJson(carga: Carga) {
    const today = new Date();
    this.carga.fecha = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    this.carga.hora = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return JSON.stringify(carga);
  }

  logoCarga = "/assets/icon/carga-uful-logo-azul.svg"

  cargaClicked(){
    this.logoCarga = "/assets/icon/carga-uful-logo-azul.svg"
    console.log("azul")
  }

  anyOtherClicked(){
    console.log("otro")
    this.logoCarga = "/assets/icon/carga-uful-logo.svg"
  }


  constructor(private route: ActivatedRoute, private dataService: DataService, private router : Router, private precios: PreciosService, private alertController : AlertController) { }

  pinFormatter(value: number) {
    return `${value} L`;
  }
  updateLitros(event: any) {
    this.carga.litros = event.detail.value;
    this.carga.total = (parseInt(this.carga.litros) * this.precioUful).toFixed(2).toString();
    this.updateTotal();
  }
  updateCantidad(event: any) {
    this.carga.total = event.detail.value.toFixed(2).toString();
    this.carga.litros = Math.round(parseInt(this.carga.total) / this.precioUful).toString();
    this.updateTotal();
  }

  continuar(){
    if(this.carga.litros == "0"){
      this.presentErrorAlert("Ingresa una cantidad de litros válida");
      return;
    }
    if(this.carga.total == "0"){
      this.presentErrorAlert("Ingresa una cantidad de dinero válida");
      return;
    }
    console.log("continuar a buscar estacion con carga: ", this.carga)
    this.dataService.setData(10,this.cargaToJson(this.carga));
    this.router.navigateByUrl('/buscar-estacion/10');
  }


  ngOnInit() {

  console.log("ngOnInit Carga precios: ", this.precioUful, this.carga.litros, this.carga.total, this.total )

    if(this.route.snapshot.data['special']){
      console.log("getting carga from data @ ingresa-carga.page.ts")
      const cargaString = this.route.snapshot.data['special'];
      this.carga = JSON.parse(cargaString);
    }
    console.log("ngOnInit tipo " + this.carga.auto.fuel)
    switch (this.carga.auto.fuel) {
      case "Regular":
        console.log("Regular", this.precios.getPrecioMagna());
        this.precioUful = this.precios.getPrecioMagna(); break;
      case "Premium":
        this.precioUful = this.precios.getPrecioPremium(); break;
      case "Diesel":
        this.precioUful = this.precios.getPrecioDiesel(); break;
      default:
        this.precioUful = this.precios.getPrecioPremium(); break;
    }


    this.carga.litros = "20";
    this.carga.total = (parseInt(this.carga.litros) * this.precioUful).toString();
    console.log("precioUful: ", this.precioUful);
  }

  async presentErrorAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Espera!',
      subHeader: msg,
      buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
    });
    await alert.present();
  }

}



















/* sirve para recibir datos por url

*/
