import { Component, OnInit } from '@angular/core';


import { ToastController } from '@ionic/angular';
import { RegistroService } from 'src/app/services/registro.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  //funcion para mostrar alertas
  async presentToastAlert(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      position: 'top'
    });

    await toast.present();
  }


  //logica mostrar/ocultar contraseña
  showPassword = false;
  typePassword = 'password';
  toogleShowPassword() {
    this.showPassword = !this.showPassword;
    this.typePassword = this.showPassword ? 'text' : 'password';
  }


  constructor(private toastController: ToastController, private registroService: RegistroService) { }

  //variables para guardar los datos del formulario
  nombre: string = '';
  email: string = '';
  password: string = '';

  ngOnInit() {

  }


  registrar() : boolean{
    //valida que los campos no esten vacios
    if(this.nombre === ''){
      this.presentToastAlert('El nombre es obligatorio');
      return false;
    }
    else if (this.email === ''){
      this.presentToastAlert('El email es obligatorio');
      return false;
    }
    else if(this.email.indexOf('@') === -1){
      this.presentToastAlert('El email no es válido');
      return false;
    }
    else if (this.password === ''){
      this.presentToastAlert('La contraseña es obligatoria');
      return false;
    }
    else if( this.password.length < 8){
      this.presentToastAlert('La contraseña debe tener al menos 8 caracteres');
      return false;
    }
    else{
      //inicia petición a la API para registrar usuario
      console.log(this.nombre);
      console.log(this.email);
      console.log(this.password);
      this.registroService.postNuevoUsuario(this.nombre, this.email, this.password)
      return true;
    }
  }
}
