import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  logoCarga = "/assets/icon/carga-uful-logo-azul.svg"

  cargaClicked(){
    this.logoCarga = "/assets/icon/carga-uful-logo-azul.svg"
    console.log("azul")
  }

  anyOtherClicked(){
    console.log("otro")
    this.logoCarga = "/assets/icon/carga-uful-logo.svg"
  }

  constructor() {}

}
