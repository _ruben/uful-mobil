import { Component, Input, OnInit } from '@angular/core';
import { Auto } from 'src/app/interfaces';
import { AutosService } from 'src/app/services/autos.service';
import { Router } from '@angular/router';
import { SeleccionColorComponent } from 'src/app/components/seleccion-color/seleccion-color.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-editar-auto',
  templateUrl: './editar-auto.page.html',
  styleUrls: ['./editar-auto.page.scss'],
})
export class EditarAutoPage implements OnInit {
  years : string [] = ["2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021","2022", "2023"]

  @Input() auto: Auto = {
    year: '',
    nickname: '',
    car_model: '',
    license_plates: '',
    fuel: '',
    state: '',
    brand: '',
    favorite: false
  };

  constructor(private autoService: AutosService, private router: Router, private modalCtrl : ModalController) { }

  ngOnInit() {
    const navigation = this.router.getCurrentNavigation();
    if(navigation && navigation.extras && navigation.extras.state) {
      this.auto = navigation.extras.state['auto'];
      console.log("Auto: ", this.auto);
    }
  }


  colorTxt = ''
  async openModalColor() {
    const modal = await this.modalCtrl.create({
      component: SeleccionColorComponent,
      breakpoints: [0, .8],
      initialBreakpoint: 0.8,
      handle: false,
      mode: 'ios',
    });
    modal.onDidDismiss().then((result) => {
      console.log(result," Llego al dismiss en nuevo auto");
      if (result.data.role === 'selected') {
        const selectedOption = result.data;
        console.log('Opción seleccionada:', selectedOption.data);
        this.auto.color = selectedOption.data.value;
        this.colorTxt = selectedOption.data.name;
      }
    });
    return await modal.present();
  }

  confirmar(){
    if(this.auto !== undefined) this.autoService.putAuto(this.auto);
    else console.error("No se puede editar un auto sin ID");
  }

}
