import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditarAutoPage } from './editar-auto.page';

describe('EditarAutoPage', () => {
  let component: EditarAutoPage;
  let fixture: ComponentFixture<EditarAutoPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditarAutoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
