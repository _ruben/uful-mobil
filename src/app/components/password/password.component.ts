import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
})
export class PasswordComponent  implements OnInit {
  name: string;
  password: any;
  constructor() {
    this.name = 'Contraseña';
  }


  @Output() contrasenaCambiada: EventEmitter<string> = new EventEmitter();

  ngOnInit() {}

  showPassword = false;
  typePassword = 'password';
  toogleShowPassword() {
    this.showPassword = !this.showPassword;
    this.typePassword = this.showPassword ? 'text' : 'password';
  }

}
