import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-seleccion-estado',
  templateUrl: './seleccion-estado.component.html',
  styleUrls: ['./seleccion-estado.component.scss'],
})
export class SeleccionEstadoComponent  implements OnInit {
  @Output() stateSelected = new EventEmitter<string>();
  selectState(_t13: string) {
    console.log("selectState: ", _t13)
    this.stateSelected.emit(_t13);
    this.closeModalEstado(_t13);
  }

  closeModalEstado(_t13: string) {
    console.log("closeModalEstado: ",_t13)
    this.modalController.dismiss({
      role: 'selected',
      data: _t13
    });
  }
  constructor(private modalController: ModalController) { }

  ngOnInit() {}

  states: string[] = [
    'Aguascalientes',
    'Baja California',
    'Baja California Sur',
    'Campeche',
    'Chiapas',
    'Chihuahua',
    'Ciudad de México',
    'Coahuila',
    'Colima',
    'Durango',
    'Estado de México',
    'Guanajuato',
    'Guerrero',
    'Hidalgo',
    'Jalisco',
    'Michoacán',
    'Morelos',
    'Nayarit',
    'Nuevo León',
    'Oaxaca',
    'Puebla',
    'Querétaro',
    'Quintana Roo',
    'San Luis Potosí',
    'Sinaloa',
    'Sonora',
    'Tabasco',
    'Tamaulipas',
    'Tlaxcala',
    'Veracruz',
    'Yucatán',
    'Zacatecas'
  ];

}
