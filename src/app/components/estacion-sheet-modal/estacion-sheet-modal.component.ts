import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Pedido } from '../../interfaces/index';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-estacion-sheet-modal',
  templateUrl: './estacion-sheet-modal.component.html',
  styleUrls: ['./estacion-sheet-modal.component.scss'],
})
export class EstacionSheetModalComponent  implements OnInit {
  seleccionado() {
    console.log(this.pedido)
    this.pedidoNuevo.setData(10, JSON.stringify(this.pedido));
    this.router.navigate(['/resumen/10']);
  }
  @Input() pedido: Pedido = {
    estacion: {
      nombre: '',
      logo: '',
      direccion: {
        ubicacion: { lat: 0, lng: 0 },
        calle: '',
        numero: '',
        colonia: '',
        ciudad: '',
        estado: '',
        codigoPostal: '',
      },
      calificaciones: [],
      tiempo: '',
      distancia: '',
      servicios: [],
    },
    carga: {
      status: '',
      auto : {
        car_model: '', license_plates: '', fuel: '',
        year: '',
        nickname: '',
        state: '',
        brand: '',
        favorite: false
      },
      fecha: '',
      hora: '',
      litros: '',
      total: '',
      tipoDeServicio: true,
    }
  };




  constructor(private router: Router, private modalCtrl: ModalController, private pedidoNuevo: DataService) { }

  ngOnInit() {
  }
  close() {
    this.modalCtrl.dismiss();
  }


}
