import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-seleccion-color',
  templateUrl: './seleccion-color.component.html',
  styleUrls: ['./seleccion-color.component.scss'],
})
export class SeleccionColorComponent  implements OnInit {

  @Output() colorSeleccionado = new EventEmitter<{}>();

selectColor(arg0: { name: string; value: string; }) {
  console.log("selectColor: ", arg0)
  this.colorSeleccionado.emit(arg0);
  this.closeModal(arg0);
}
closeModal(arg0: { name: string; value: string; }) {
  console.log("closeModalColor: ",arg0)
  this.modalController.dismiss({
    role: 'selected',
    data: arg0
  });
}

  colors = [
    {name: 'Rojo', value: '#ff0000'},
    {name: 'Verde', value: '#00ff00'},
    {name: 'Azul', value: '#0000ff'},
    {name: 'Amarillo', value: '#ffff00'},
    {name: 'Cyan', value: '#00ffff'},
    {name: 'Magenta', value: '#ff00ff'},
    {name: 'Plata', value: '#c0c0c0'},
    {name: 'Gris', value: '#808080'},
    {name: 'Blanco', value: '#ffffff'},
    {name: 'Negro', value: '#000000'},
  ]

  constructor(private modalController: ModalController) { }

  ngOnInit() {}

}
