import { Component, Inject, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { GooglemapsService } from 'src/app/services/googlemaps.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-mapa-estacion-sheet',
  templateUrl: './mapa-estacion-sheet.component.html',
  styleUrls: ['./mapa-estacion-sheet.component.scss'],
  providers: [{ provide: DOCUMENT, useValue: document }]
})
export class MapaEstacionSheetComponent  implements OnInit {


  constructor(
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any,
    private googlemapsService: GooglemapsService,
    private modalCtrl: ModalController
    ) { }

  @Input() userPosition = {
    lat: 19.0466335310171,
    lng: -98.20842362880778,
  }
  @Input() estacionPosition = {
    lat: 19.03491005210532,
    lng: -98.22680034506503,
  }
  map: any;
  userMarker: any;
  estacionMarker: any;

  @ViewChild('mapElement') divMap: any | undefined;

  ngOnInit() {
    this.init();
  }

  async init(){
    this.googlemapsService.init(this.renderer, this.document).then( () => {
      this.initMap();
    }).catch((err: any) => {
      console.log(err);
    });
  }

  initMap() {

    let latLngUser = new google.maps.LatLng(this.userPosition.lat, this.userPosition.lng);
    let mapOptions ={
      center: latLngUser,
      zoom: 13,
      disableDefaultUI: true,
      clickeableIcons: true,
    }

    this.map = new google.maps.Map(this.divMap?.nativeElement, mapOptions);
    this.userMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable: false,
      position: latLngUser,
      // icon: {
      //   url: 'assets/icon/user.png',
      //   scaledSize: new google.maps.Size(50, 50),
      // },
    });

    let latLngEstacion = new google.maps.LatLng(this.estacionPosition.lat, this.estacionPosition.lng);

    this.estacionMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable: false,
      position: latLngEstacion,
      // icon: {
      //   url: 'assets/icon/estacion.png',
      //   scaledSize: new google.maps.Size(50, 50),
      // },
    });


  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}


