import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-boton-animado',
  templateUrl: './boton-animado.component.html',
  styleUrls: ['./boton-animado.component.scss'],
})
export class BotonAnimadoComponent  implements OnInit {
  @Input() tipo: string = ''
  constructor() { }

  ngOnInit() {
  }

}
