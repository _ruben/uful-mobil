import { Component, Input, OnInit } from '@angular/core';
import { TicketDeCarga } from 'src/app/interfaces';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-ticket2',
  templateUrl: './ticket2.component.html',
  styleUrls: ['./ticket2.component.scss'],
})
export class Ticket2Component  implements OnInit {

  constructor(private launchNavigator: LaunchNavigator ) { }

  @Input() qr : string = '';
  @Input() pedido: TicketDeCarga = {
    _id: '',
    ticket_folio: null,
    liters: 0,
    liters_loaded: 0,
    exact_charge: null,
    price_per_liter: 0,
    total: 0,
    total_charged: 0,
    car_id: '',
    status: '',
    customer_id: '',
    ticket_number: '',
    created_date: undefined,
    created_time: '',
    created_timestamp: 0,
    createdAt: undefined,
    updatedAt: undefined,
    fuel: {
      _id: '',
      name: '',
      type: '',
    }
  };
  @Input() start: {
    lat: number ;
    lng: number ;
  } | undefined
  @Input() end: {
    lat: number,
    lng: number,
  } | undefined

  ngOnInit() {

  }

  openNavigation() {
    console.log('openNavigation', this.start, this.end);
    if (this.start && this.end) {
      console.log(this.start);
      console.log(this.end);
      const options: LaunchNavigatorOptions = {
        appSelection: {
          dialogHeaderText: 'Selecciona una app para navegar',
        },
        app: this.launchNavigator.APP.ANY,
        // app: this.launchNavigator.APP.ANY,
        // start: [this.lat, this.lng],
      };
      this.launchNavigator.navigate([this.start.lat, this.start.lng], options).then(
        (success) => {
          console.log(success);
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

}
