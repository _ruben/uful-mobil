import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { TicketDeCarga } from 'src/app/interfaces';



@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
})
export class TicketComponent {

  horas: number = 4;
  minutos: number = 0;
  segundos: number = 0;

  isActionSheetOpen = false;
  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: [{
        text:'OK', handler: () => {
          window.location.reload();
        }
      },],
      mode: 'ios',
      backdropDismiss: false
    });

    await alert.present();
  }
  public actionSheetButtons = [
    {
      text: 'Sí, cancelar Ticket',
      icon: 'trash-bin-outline',
      handler: () => {
        console.log('Confirm clicked');
        localStorage.removeItem('qr');
        localStorage.removeItem('autoID');
        localStorage.removeItem('cargas');
        this.presentAlert('Ticket cancelado');

        this.isActionSheetOpen = false;
      }
    },
    {
      text: 'Regresar',
      icon: 'arrow-back-outline',
      role: 'close',
      handler: () => {
        console.log('Cancel clicked');
        this.isActionSheetOpen = false;
      }
    }
  ]
  async confirmarCancelarTicket() {
    this.isActionSheetOpen = true;
    console.log('Confirm Cancelar Ticket');
    const actionSheet = await this.actionSheetController.create({
      header: 'Confirma que deseas cancelar el ticket. Podria haber cargos adicionales si lo haces fuera del tiempo establecido.Consulta las condiciones de cancelación en la sección de ayuda',
      buttons: this.actionSheetButtons,
      mode: 'ios'
    });
    await actionSheet.present();
  }

  constructor(private launchNavigator: LaunchNavigator, private actionSheetController: ActionSheetController, private alertController: AlertController, private router : Router ) {
    this.cuentaRegresiva();
  }

  cuentaRegresiva() {
    let horas = localStorage.getItem('horas');
    let minutos = localStorage.getItem('minutos');
    let segundos = localStorage.getItem('segundos');
    if (horas && minutos && segundos) {
      this.horas = parseInt(horas);
      this.minutos = parseInt(minutos);
      this.segundos = parseInt(segundos);
    }
    let now = new Date();
    let fecha = new Date(this.pedido.created_timestamp);
    let diff = now.getTime() - fecha.getTime();
    let diffHoras = diff / (1000 * 60 * 60);
    let diffMinutos = diff / (1000 * 60);
    let diffSegundos = diff / 1000;
    this.horas = 4 - Math.floor(diffHoras);
    this.minutos = 60 - Math.floor(diffMinutos);
    this.segundos = 60 - Math.floor(diffSegundos);

    let interval = setInterval(() => {
      if (this.segundos > 0) {
        this.segundos--;
      } else {
        if (this.minutos > 0) {
          this.minutos--;
          this.segundos = 59;
        } else {
          if (this.horas > 0) {
            this.horas--;
            this.minutos = 59;
            this.segundos = 59;
          } else {
            clearInterval(interval);
            this.confirmarCancelarTicket();
          }
        }
      }
      localStorage.setItem('horas', this.horas.toString());
      localStorage.setItem('minutos', this.minutos.toString());
      localStorage.setItem('segundos', this.segundos.toString());
    }, 1000);

  }

  @Input() qr : string = '';
  @Input() pedido: TicketDeCarga = {
    _id: '',
    ticket_folio: null,
    liters: 0,
    liters_loaded: 0,
    exact_charge: null,
    price_per_liter: 0,
    total: 0,
    total_charged: 0,
    car_id: '',
    status: '',
    customer_id: '',
    ticket_number: '',
    created_date: undefined,
    created_time: '',
    created_timestamp: 0,
    createdAt: undefined,
    updatedAt: undefined,
    fuel: {
      _id: '',
      name: '',
      type: '',
    }
  };
  @Input() start: {
    lat: number ;
    lng: number ;
  } | undefined
  @Input() end: {
    lat: number,
    lng: number,
  } | undefined

  ngOnInit() {

  }

  openNavigation() {
    if (this.start && this.end) {
      console.log(this.start);
      console.log(this.end);
      const options: LaunchNavigatorOptions = {
        start: [this.start.lat, this.start.lng],
        app: this.launchNavigator.APP.USER_SELECT
      };

      this.launchNavigator.navigate([this.end.lat, this.end.lng], options)
        .then(
          success => console.log('Launched navigator'),
          error => console.log('Error launching navigator', error)
        );
    }
  }

}
