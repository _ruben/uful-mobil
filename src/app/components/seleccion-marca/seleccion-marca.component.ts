import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-seleccion-marca',
  templateUrl: './seleccion-marca.component.html',
  styleUrls: ['./seleccion-marca.component.scss'],
})
export class SeleccionMarcaComponent  implements OnInit {
  constructor(private modalController: ModalController) { }

  @Output() marcaSeleccionada = new EventEmitter<string>();

  seleccionarMarca(arg0: string) {
    console.log("seleccionarMarca: ", arg0)
    this.marcaSeleccionada.emit(arg0);
    this.closeModalMarca(arg0);
  }

  closeModalMarca(arg0: string) {
    console.log("closeModalMarca: ", arg0)
    this.modalController.dismiss({
      role: 'selected',
      data: arg0
    });
  }


  ngOnInit() {}

}
