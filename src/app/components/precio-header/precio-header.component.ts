import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Direccion } from 'src/app/interfaces';
import { PreciosService } from 'src/app/services/precios.service';

@Component({
  selector: 'app-precio-header',
  templateUrl: './precio-header.component.html',
  styleUrls: ['./precio-header.component.scss'],
})
export class PrecioHeaderComponent  implements OnInit {
  @Input() tipo: string = "Regular";
  @Input() direccion: Direccion = {
    ubicacion: { lat: 0, lng: 0 },
    calle: '',
    numero: '',
    colonia: '',
    ciudad: '',
    estado: '',
    codigoPostal: '',
  };
  precioRegular: number = 0;
  precioPremium: number = 0;
  precioDiesel: number = 0;


  constructor(private precios: PreciosService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.direccion.estado = this.activatedRoute.snapshot.paramMap.get('direccion') || '';
    console.log("Direccion: ", this.direccion);
    this.obtenerPreciosGasolina();
  }

  ionViewWillEnter() {
    this.obtenerPreciosGasolina();
  }

  toFixedNumber(num:number, digits:number = 2 , base: number = 10){
    var pow = Math.pow(base||10, digits);
    return Math.round(num*pow) / pow;
  }


  obtenerPreciosGasolina() {
    try{
      console.log("Municipio: ", this.direccion.ciudad);
      this.precios.getPrecios(this.direccion.ciudad).subscribe(
        precios => {
          // Asigna los precios a las variables correspondientes en tu componente
          this.precioRegular = this.toFixedNumber(precios.regular);
          console.log("precioRegular: ", this.precioRegular);
          this.precioPremium = this.toFixedNumber(precios.premium);
          console.log("precioPremium: ", this.precioPremium);
          this.precioDiesel = this.toFixedNumber(precios.diesel);
          console.log("precioDiesel: ", this.precioDiesel);
        },
        error => {
          // Maneja los errores
          console.error(error);
        }
      );
    }catch(error){
      console.error(error);
    }
  }
}
