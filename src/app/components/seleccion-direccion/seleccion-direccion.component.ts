import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-seleccion-direccion',
  templateUrl: './seleccion-direccion.component.html',
  styleUrls: ['./seleccion-direccion.component.scss'],
})
export class SeleccionDireccionComponent  implements OnInit {
  @Output() direccionSeleccionada = new EventEmitter<string>();
  constructor(private router: Router, private modalController: ModalController) { }

  ngOnInit() {}

  mapa(){
    console.log("desplegando mapa");
    this.direccionSeleccionada.emit("mapa");
    this.closeModalDireccion("mapa");
  }
  actual(){
    console.log("ubicacion actual");
    this.direccionSeleccionada.emit("actual");
    this.closeModalDireccion("actual");
  }
  closeModalDireccion(arg0: string) {
    console.log("closeModalDireccion: ", arg0)
    this.modalController.dismiss({
      role: 'selected',
      data: arg0
    });
  }

}
