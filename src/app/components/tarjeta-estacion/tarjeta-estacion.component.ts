import { Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { ModalController } from '@ionic/angular';

import { EstacionSheetModalComponent } from '../estacion-sheet-modal/estacion-sheet-modal.component';
import {  Pedido } from '../../interfaces/index';

@Component({
  selector: 'app-tarjeta-estacion',
  templateUrl: './tarjeta-estacion.component.html',
  styleUrls: ['./tarjeta-estacion.component.scss'],
})
export class TarjetaEstacionComponent  implements OnInit {

  @ViewChild('cardContent', { static: false }) cardContent!: ElementRef;

  @Input() pedido: Pedido = {
    estacion: {
      nombre: '',
      logo: '',
      direccion: {ubicacion: { lat: 0, lng: 0 },calle:'', numero:'', colonia:'', ciudad:'', estado:'', codigoPostal:''},
      calificaciones: [],
      tiempo: '',
      distancia: '',
      servicios: []
    },
    carga: {
      status: 'En proceso',
      auto :  {
        car_model: '', license_plates: '', fuel: '',
        year: '',
        nickname: '',
        state: '',
        brand: '',
        favorite: false
      },
      fecha: '',
      hora: '',
      total: '',
      litros: '',
      tipoDeServicio: true,
    }

  }

  calificacionPromedio: number = 0;



constructor( private modalCtrl: ModalController,) {
  }

  @Input() clickeable: boolean = true;

  async presentModal(pedido: Pedido) {
    if(this.clickeable){
      this.pedido = pedido;
      console.log("estacion sheet component sendindg: ", this.pedido)
      const modal = await this.modalCtrl.create({
        component: EstacionSheetModalComponent,
        breakpoints: [0, .5, 1],
        initialBreakpoint: 0.5,
        handle: false,
        componentProps: {
          pedido: this.pedido //manda la data que se recibe desde Input en EstacionSheetModalComponent
        },
      });
      await modal.present();
    }
  }

  ngOnInit() {
    this.calificacionPromedio = this.getPromedioDe(
      this.pedido.estacion.calificaciones.map(calificacion => calificacion.valoracion))
  }

  ionViewDidEnter() {
  }


  private getPromedioDe(list:number[]){
    const promedio = list.reduce((a, b) => a + b, 0) / list.length;
    return this.redondearA2Decimales(promedio);
  }

  private redondearA2Decimales(numero: number){
    return Math.round(numero * 100) / 100;
  }

}
