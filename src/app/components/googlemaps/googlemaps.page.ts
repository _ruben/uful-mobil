import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { GooglemapsService } from 'src/app/services/googlemaps.service';

@Component({
  selector: 'app-googlemaps',
  templateUrl: './googlemaps.page.html',
  styleUrls: ['./googlemaps.page.scss'],
  providers: [{ provide: DOCUMENT, useValue: document }]
})
export class GooglemapsPage implements OnInit {

  constructor(private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any,
    private googlemapsService: GooglemapsService) { }

  @Input() position = {
    lat: 19.0466335310171,
    lng: -98.20842362880778,
  }


  label ={
    titulo: '',
    subtitulo : ''
  }

  map: any;
  userMarker: any;
  estacionesMarkers: any[] = [];
  infoWindow: any;
  positionSet : any;

  @ViewChild('mapElement') divMap: ElementRef | undefined;

  ngOnInit() {
    this.init();
  }

  async init(){
    this.googlemapsService.init(this.renderer, this.document).then( () => {
      this.initMap();
    }).catch((err: any) => {
      console.log(err);
    });
  }

  initMap() {
    const position = this.position;
    let latLng = new google.maps.LatLng(position.lat, position.lng);

    let mapOptions ={
      center: latLng,
      zoom: 14,
      disableDefaultUI: true,
      clickeableIcons: true,
    }

    this.map = new google.maps.Map(this.divMap?.nativeElement, mapOptions);
    this.userMarker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable: true,
    });

    this.clickHandleEvent();
    this.infoWindow = new google.maps.InfoWindow();

    this.addMarker(this.position);
    this.setInfoWindow(this.userMarker,this.label);


  }

  clickHandleEvent(){
    this.map.addListener('click', (event: any) => {
      const position = {
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
      }
      this.addMarker(position);
    });
  }

  addMarker(position: any){
    let latLng = new google.maps.LatLng(position.lat, position.lng);
    this.userMarker.setPosition(latLng);
    this.map.panTo(latLng);
    this.positionSet = position;
  }

  setInfoWindow(marker: any, label: any){

    const contentString =
      '<div id="content">'+
        '<div>'+
          '<h1>' + label.titulo + '</h1>'+
          '<p>' + label.subtitulo + '</p>'+
        '</div>'+
        '<div>'+
          '<button id="btnAceptar">Seleccionar</button>'+
        '</div>'+
      '</div>';
    this.infoWindow.setContent(contentString);
    this.infoWindow.open(this.map, marker);
  }


}
