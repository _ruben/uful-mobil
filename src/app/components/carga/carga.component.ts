import { Component, Input, OnInit } from '@angular/core';
import { Carga, Estacion } from '../../interfaces/index';

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styleUrls: ['./carga.component.scss'],
})
export class CargaComponent  implements OnInit {


  @Input() carga: Carga = {
    status: "Iniciando carga",
    auto : {
      car_model: '', license_plates: '', fuel: '',
      year: '',
      nickname: '',
      state: '',
      brand: '',
      favorite: false
    },
    fecha: "",
    hora: "",
    total: "",
    litros: "",
    tipoDeServicio: true,
  }

  @Input() estacion: Estacion = {
    nombre: "",
    logo: "",
    direccion: {
      ubicacion: { lat: 0, lng: 0 },
      calle: "",
      numero: "",
      colonia: "",
      ciudad: "",
      estado: "",
      codigoPostal: "",
    },
    calificaciones: [],
    tiempo: "",
    distancia: "",
    servicios: [],
  }


  constructor() {

  }

  ngOnInit() {

  }


  isToday(fecha:string){
    let today = new Date();
    let todayString = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    // console.log(todayString);
    // console.log(fecha);
    return fecha == todayString;
  }

}
