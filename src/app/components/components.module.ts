import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SocialMediaLoginComponent } from './social-media-login/social-media-login.component';
import { PasswordComponent } from './password/password.component';
import { InputComponent } from './input/input.component';
import { ItemComponent } from './item/item.component';
import { CargaComponent } from './carga/carga.component';
import { BotonAnimadoComponent } from './boton-animado/boton-animado.component';
import { PrecioHeaderComponent } from './precio-header/precio-header.component';
import { TarjetaEstacionComponent } from './tarjeta-estacion/tarjeta-estacion.component';
import { EstacionSheetModalComponent } from './estacion-sheet-modal/estacion-sheet-modal.component';
import { TicketComponent } from './ticket/ticket.component';
import { MapaEstacionSheetComponent } from './mapa-estacion-sheet/mapa-estacion-sheet.component';
import { SeleccionDireccionComponent } from './seleccion-direccion/seleccion-direccion.component';
import { SeleccionMarcaComponent } from './seleccion-marca/seleccion-marca.component';
import { SeleccionModeloComponent } from './seleccion-modelo/seleccion-modelo.component';
import { SeleccionColorComponent } from './seleccion-color/seleccion-color.component';
import { SeleccionEstadoComponent } from './seleccion-estado/seleccion-estado.component';
import { Ticket2Component } from './ticket2/ticket2.component';
import { SeleccionAutoComponent } from './seleccion-auto/seleccion-auto.component';

@NgModule({
  declarations: [
    SocialMediaLoginComponent,
    PasswordComponent,
    InputComponent,
    ItemComponent,
    CargaComponent,
    BotonAnimadoComponent,
    PrecioHeaderComponent,
    TarjetaEstacionComponent,
    EstacionSheetModalComponent,
    TicketComponent,
    MapaEstacionSheetComponent,
    SeleccionDireccionComponent,
    SeleccionMarcaComponent,
    SeleccionModeloComponent,
    SeleccionColorComponent,
    SeleccionEstadoComponent,
    Ticket2Component,
    SeleccionAutoComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [
    SocialMediaLoginComponent,
    PasswordComponent,
    InputComponent,
    ItemComponent,
    CargaComponent,
    BotonAnimadoComponent,
    PrecioHeaderComponent,
    TarjetaEstacionComponent,
    EstacionSheetModalComponent,
    TicketComponent,
    MapaEstacionSheetComponent,
    SeleccionDireccionComponent,
    SeleccionMarcaComponent,
    SeleccionModeloComponent,
    SeleccionColorComponent,
    SeleccionEstadoComponent,
    SeleccionAutoComponent,
    Ticket2Component,
  ]
})
export class ComponentsModule { }
