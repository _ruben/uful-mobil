import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Auto } from 'src/app/interfaces';
import { AutosService } from 'src/app/services/autos.service';


@Component({
  selector: 'app-seleccion-auto',
  templateUrl: './seleccion-auto.component.html',
  styleUrls: ['./seleccion-auto.component.scss'],
})
export class SeleccionAutoComponent  implements OnInit {
  @Input() autos: Auto[] | undefined;
  @Output() autoSeleccionado= new EventEmitter<Auto>();
  closeModal(auto: Auto) {
    this.modalController.dismiss({
      role:'selected',
      data: auto
    })
  }
  selectAuto(auto: Auto){
    this.autoSeleccionado.emit(auto)
    this.closeModal(auto)
  }

  constructor(private modalController: ModalController) {

  }

  ngOnInit() {
  }

}
