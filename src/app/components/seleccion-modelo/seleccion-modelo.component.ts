import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-seleccion-modelo',
  templateUrl: './seleccion-modelo.component.html',
  styleUrls: ['./seleccion-modelo.component.scss'],
})
export class SeleccionModeloComponent  implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    console.log("modelos: ", this.modelos)
  }
  @Input() modelos = [];
  @Output() modeloSeleccionado = new EventEmitter<string>();

  seleccionaModelo(arg0: string) {
    console.log("seleccionaModelo: ", arg0)
    this.modeloSeleccionado.emit(arg0);
    this.closeModalModelo(arg0);
  }

  closeModalModelo(arg0: string) {
    console.log("closeModalModelo: ", arg0)
    this.modalController.dismiss({
      role: 'selected',
      data: arg0
    });
  }


}
