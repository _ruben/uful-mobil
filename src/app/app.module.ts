import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NgCreditCardModule } from "angular-credit-card"
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistroService } from './services/registro.service';
import { PreciosService } from './services/precios.service';
import { AuthService } from './services/auth.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { UbicacionService } from './services/ubicacion.service';
import { AutosService } from './services/autos.service';
import { MetodosDePagoService } from './services/metodos-de-pago.service';
import { EstacionesService } from './services/estaciones.service';
import { QrService } from './services/qr.service';
import { EventService } from './services/event.service';
import { LoadingService } from './services/loading.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';




@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, BrowserAnimationsModule, NgCreditCardModule, HttpClientModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, AndroidPermissions, RegistroService,
    PreciosService, AuthService, Geolocation, UbicacionService, AutosService, MetodosDePagoService, EstacionesService, QrService, EventService, LoadingService, LaunchNavigator],
  bootstrap: [AppComponent],
})
export class AppModule {}
