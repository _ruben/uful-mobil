import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PreciosService {

  constructor(private http: HttpClient) {
  }


  // token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NDE5NGNiODg5NTM0NDA2ZTQ2ZThmZSIsInJvbGUiOnsiX2lkIjoiNjJlYzVlZmNkMzMzMTQ1N2Y2YWVkMjZmIiwibmFtZSI6InVzZXIifSwiaWF0IjoxNjgyMzk3OTc2LCJleHAiOjE2ODMwMDI3NzZ9.l1VW1ak3h2xJojhk-bTEP8om9e2fM2QYlQLIqPby0bo";

  precioRegular: number = 0; //magna
  precioPremium: number = 0; //premium
  precioDiesel : number = 0; //diesel

  public getPrecios(municipio: string = 'Atlixco'): Observable<any> {
    console.log("Inició la petición", municipio);
    if(municipio === ""){
      municipio = "Atlixco"
    }
    if(municipio === "Heroica Puebla de Zaragoza"){
      municipio = "Puebla"
    }
    const url = `https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/gasStations/getUfulPrices/${municipio}`;
    const auth =  localStorage.getItem('token') //lee el token del localstorage //TODO; cambiar por el token del usuario logueado
    if( auth == null){
      console.log("No hay token");
      return throwError("No hay token");
    }
    console.log("Token: ", auth);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': auth
    });
    return this.http.get(url, { headers }).pipe(
      map((response: any) => {
        // Obtén los precios de gasolina del array de data en la respuesta
        const data = response.data;
        this.precioRegular = data[0].PVP ;
        this.precioPremium = data[1].PVP ;
        this.precioDiesel = data[2].PVP ;//data[2].PVP;
        // Crea un objeto con los precios de gasolina
        const precios = {
          regular:this.precioRegular,
          premium:this.precioPremium,
          diesel:this.precioDiesel
        };
        console.log("Precios from service: ", precios);

        // Devuelve los precios como un observable
        return precios;
      }),
      catchError((error: any) => {
        // Maneja los errores en caso de que ocurran
        console.log('Error en la petición de precios de gasolina:', error);
        return Observable.throw(error);
      })
    );
  }

  public getPrecioMagna(): number {
    return this.precioRegular;
  }
  public getPrecioPremium(): number {
    return this.precioPremium;
  }
  public getPrecioDiesel(): number {
    return this.precioDiesel;
  }
}

