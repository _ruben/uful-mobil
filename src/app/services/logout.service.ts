import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private http: HttpClient, private router: Router) { }

  public getLogout() : any{
    const auth = localStorage.getItem('token');
    if( auth == null){
      console.log("No hay token");
      return throwError("No hay token");
    }

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': auth
    })
    const url = `https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/user/signOut`;
    this.http.get(url, { headers }).subscribe((response: any) => {
      console.log(response);
      localStorage.removeItem('user-data');
      localStorage.removeItem('token');
      this.router.navigate(['/login']);
    },(error: any) => {
      console.log(error);
    }
    );
  }


}
