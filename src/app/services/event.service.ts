import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {



  private updateSubject = new Subject<any>();
  update$ = this.updateSubject.asObservable();
  updateData(data: any) {
    this.updateSubject.next(data);
  }

}
