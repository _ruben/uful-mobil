import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MetodosDePagoService {

  constructor() { }

  indexSeleccionado: number = 0;



  tarjetas = [
    {
      nombreTitular: "Alejandro",
      apellidoTitular: "Sánchez",
      icon: "../../../assets/icon/mastercard-icon.svg",
      terminacion: '1234',
    },
    {
      nombreTitular: "Alejandro",
      apellidoTitular: "Sánchez",
      icon: "../../../assets/icon/mastercard-icon.svg",
      terminacion: '5678',
    },
    {
      nombreTitular: "Alejandro",
      apellidoTitular: "Sánchez",
      icon: "../../../assets/icon/mastercard-icon.svg",
      terminacion: '9012',
    },
  ]

  public getDefault(){
    return this.tarjetas[this.indexSeleccionado];
  }

  public getMetodosDePago() : any[]{
    return this.tarjetas;
  }

}
