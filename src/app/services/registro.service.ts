import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

/**
 * tokens de users de pruebas:
 * "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NDE5NGNiODg5NTM0NDA2ZTQ2ZThmZSIsInJvbGUiOnsiX2lkIjoiNjJlYzVlZmNkMzMzMTQ1N2Y2YWVkMjZmIiwibmFtZSI6InVzZXIifSwiaWF0IjoxNjgyMDE5NTMxLCJleHAiOjE2ODI2MjQzMzF9.7-qLrPuVgJn71Kt-MhBkOL9pvn9cu-jTei2Dgo6a3oQ"
 * "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NDFhMGE0MmZmNWNjOWFlMjE4YTQ1MSIsInJvbGUiOnsiX2lkIjoiNjJlYzVlZmNkMzMzMTQ1N2Y2YWVkMjZmIiwibmFtZSI6InVzZXIifSwiaWF0IjoxNjgyMDIyNTY1LCJleHAiOjE2ODI2MjczNjV9.XTLjof1HNh7DVGAiaTf407GRwt7IJ8YLXuNg1ec7g6k"
 * "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NDE5NGNiODg5NTM0NDA2ZTQ2ZThmZSIsInJvbGUiOnsiX2lkIjoiNjJlYzVlZmNkMzMzMTQ1N2Y2YWVkMjZmIiwibmFtZSI6InVzZXIifSwiaWF0IjoxNjgyMzk3OTc2LCJleHAiOjE2ODMwMDI3NzZ9.l1VW1ak3h2xJojhk-bTEP8om9e2fM2QYlQLIqPby0bo"
 */


async presentConfirmAlert() {
  const alert = await this.alertController.create({
    header: 'Hecho!',
    subHeader: 'Tu cuenta ha sido creada con éxito',

    buttons: [{
  text: 'OK',
  role: 'confirm',
  handler: () => {  this.router.navigate(['/tabs/carga']); }
}],
  });

  await alert.present();
}

async presentErrorAlert(msg: string) {
  const alert = await this.alertController.create({
    header: 'Error!',
    subHeader: msg,
    buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
  });
  await alert.present();
}


  constructor(private http: HttpClient, private alertController: AlertController, private router: Router) { }

  public postNuevoUsuario(nombre: string, email: string, password: string) : any{
    const url = 'https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/auth/signUp';
    const body = {
      name: nombre,
      email: email,
      password: password
    }
    console.log("registrando usuario...");
    this.http.post(url, body, {
      headers: {
        'Content-Type': 'application/json',
      }
    }).subscribe(
      (response: any) => {
        console.log("respuesta exitosa:", response); // Maneja la respuesta exitosa
        //guarda la data del usuario en el storage
        localStorage.setItem("user-data", JSON.stringify(response.data.user));
        //guarda el token en el storage
        localStorage.setItem('token', response.data.Token); // guarda el token en el storage
        this.presentConfirmAlert();
      },
      (error: HttpErrorResponse) => {
        console.error("respuesta errorosa", error); // Maneja el error
        this.presentErrorAlert(error.error.data.errors[0].msg);
      }
    );
  }

}
