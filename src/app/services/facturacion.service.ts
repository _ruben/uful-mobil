import { Injectable } from '@angular/core';
import { DatoFiscal } from '../interfaces';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FacturacionService {

  constructor(private http: HttpClient) { }

  baseURL = 'https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/user'

  postFacturacion(datos: DatoFiscal) {
    console.log("Guardando... ", datos);
    const url = `${this.baseURL}/fiscalData/registerFiscalData`;
    const body = {
      rfc: datos.rfc,
      razonSocial: datos.razonSocial,
      domicilioFiscal: datos.domicilioFiscal,
      regimenFiscal: datos.regimenFiscal,
      usoCFDI: datos.usoCFDI
    };
    const authToken = localStorage.getItem('token');
    if(authToken == null) {
      console.log("No hay token");
      return;
    }
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    console.log("toke:", authToken);
    return this.http.post(url, body, {headers}).subscribe((data) => {
      console.log("Datos fiscales registrados: ", data);
    }, (error) => {
      console.log("Error al registrar datos fiscales: ", error);
    });


  }


}
