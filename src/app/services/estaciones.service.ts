import { Injectable } from '@angular/core';
import { Estacion } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class EstacionesService {

  iconPemex = '../../../assets/img/pemex.png';
  iconSuperCarga = '../../../assets/img/supercarga-logo.svg';

  estaciones: Estacion[] = [
    {
      nombre : 'Estacion bp 1',
      logo: '../../../assets/img/supercarga-logo.svg',
      direccion: {
        ubicacion: { lat: 19.06741150243431,  lng: -98.21416953894911 },
        calle: 'Blvrd Nte Esquina',
        numero: 'Esquina',
        ciudad: 'Puebla',
        colonia: 'Héroes Del 5 De Mayo',
        estado: 'Pue.',
        codigoPostal: '72000'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.2
      },
      {
        nombre: "atencion",
        valoracion:3.9
      },
      {
        nombre: "limpieza",
        valoracion:2.0
      }],
      tiempo: '10 min',
      distancia: '1 km',
      servicios: ['storefront']
    },
    {
      nombre: 'Estacion Pemex 1',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat:19.054756053722304, lng:-98.2134828934467 },
        calle: 'Av 2 Pte N',
        numero: '2113',
        ciudad: 'Puebla',
        colonia: 'Amor',
        estado: 'Pue',
        codigoPostal: '72140'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.1
      },
      {
        nombre: "atencion",
        valoracion:4.3
      },
      {
        nombre: "limpieza",
        valoracion:4.8
      }],
      tiempo: '20 min',
      distancia: '2 km',
      servicios: ['cafe']
    },
    {
      nombre: 'Gasolinera los Angeles sa de cv',
      logo: '../../../assets/img/supercarga-logo.svg',
      direccion: {
        ubicacion: { lat: 19.028458080643137,   lng: -98.20846444500269},
        calle: 'C. 16 de Septiembre',
        numero: '4322',
        ciudad: 'Puebla',
        colonia: 'Huexotitla',
        estado: 'Pue.',
        codigoPostal: '72534',
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.4
      },
      {
        nombre: "atencion",
        valoracion:4.5
      },
      {
        nombre: "limpieza",
        valoracion:4.6
      }],
      tiempo: '30 min',
      distancia: '3 km',
      servicios: ['cafe', 'wifi', 'storefront']
    },
    {
      nombre: 'Gasolinera El Trébol',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 19.00566898868342, lng: -98.20295586073532 },
        calle: 'Blvd. Capitán Carlos Camacho Espíritu',
        numero: '6305',
        ciudad: 'Puebla',
        colonia: 'Villa Universitaria',
        estado: 'Pue.',
        codigoPostal: '72589'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.3
      },
      {
        nombre: "atencion",
        valoracion:4.1
      },
      {
        nombre: "limpieza",
        valoracion:4.0
      }],
      tiempo: '40 min',
      distancia: '4 km',
      servicios: ['cafe', 'wifi']
    },
    {
      nombre: 'Estacion 5',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat:19.03881760195747, lng:-98.22481352405647 },
        calle: 'Av 41 Pte',
        numero: '2117',
        ciudad: 'Puebla',
        colonia: 'La Noria',
        estado: 'Pue.',
        codigoPostal: '72410'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.0
      },
      {
        nombre: "atencion",
        valoracion:4.2
      },
      {
        nombre: "limpieza",
        valoracion:4.1
      }],
      tiempo: '50 min',
      distancia: '5 km',
      servicios: ['cafe', 'wifi', 'storefront']
    },
    {
      nombre: 'Las Americas',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 19.061690863067927,  lng: -98.27945043829183 },
        calle: 'Ruta Quetzalcóatl',
        numero: '1',
        ciudad: 'San Andrés Cholula',
        colonia: 'Barrio Real',
        estado: 'Pue.',
        codigoPostal: '72810'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.1
      },
      {
        nombre: "atencion",
        valoracion:4.6
      },
      {
        nombre: "limpieza",
        valoracion:4.7
      }],
      tiempo: '10 min',
      distancia: '1.2 km',
      servicios: ['cafe', 'storefront', 'pizza-outline']
    },
    {
      nombre: 'Gasolinera Forjadores',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 19.03415688089882, lng: -98.2244556464523 },
        calle: 'Blvd. Forjadores de Puebla',
        numero: '1007',
        ciudad: 'Cholula,',
        colonia: 'San Miguel Tlatempa',
        estado: 'Pue',
        codigoPostal: '72760'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.7
      },
      {
        nombre: "atencion",
        valoracion:4.8
      },
      {
        nombre: "limpieza",
        valoracion:4.8
      }],
      tiempo: '70 min',
      distancia: '7 km',
      servicios: ['cafe', 'wifi', 'storefront']
    },
    {
      nombre: 'Gasolinera San Rafael Comac',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 19.039143553035476, lng: -98.31592656039449 },
        calle: 'Tonantzintla, M. Hidalgo km 1',
        numero: '2911',
        colonia: 'San Rafael Comac',
        ciudad: 'San Andrés Cholula',
        estado: 'Pue.',
        codigoPostal: '72840'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.7
      },
      {
        nombre: "atencion",
        valoracion:4.8
      },
      {
        nombre: "limpieza",
        valoracion:4.8
      }],
      tiempo: '20 min',
      distancia: '7 km',
      servicios: ['cafe', 'wifi', 'storefront']
    },
    {
      nombre: 'Repsol',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 19.056187559039248, lng: -98.32650273340984 },
        calle: 'Paso de Cortes , Llanos Sta María, , 72760 Cholula, Pue.',
        numero: '2911',
        colonia: 'Barrio de Sta Maria Xixitla',
        ciudad: 'Cholula',
        estado: 'Pue.',
        codigoPostal: '72760'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.7
      },
      {
        nombre: "atencion",
        valoracion:4.8
      },
      {
        nombre: "limpieza",
        valoracion:4.8
      }],
      tiempo: '20 min',
      distancia: '7 km',
      servicios: ['cafe', 'wifi', 'storefront']
    },
    {
      nombre: 'GEMMA-ATLIXCO',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 18.903261764872727, lng: -98.41785475134208 },
        calle: '110, Carr. Internacional ,  , Pue.',
        numero: '110',
        colonia: 'Barrio de Sta Maria Xixitla',
        ciudad: 'Atlixco',
        estado: 'Pue.',
        codigoPostal: '74240'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.7
      },
      {
        nombre: "atencion",
        valoracion:4.8
      },
      {
        nombre: "limpieza",
        valoracion:4.8
      }],
      tiempo: '20 min',
      distancia: '7 km',
      servicios: ['cafe', 'wifi', 'storefront']
    },
    {
      nombre: 'Gasolinera Pemex',
      logo: this.iconPemex,
      direccion: {
        ubicacion: { lat: 18.905659940403414, lng: -98.43807079692502 },
        calle: '110, Carr. Internacional ,  , Pue.',
        numero: '110',
        colonia: 'Barrio de Sta Maria Xixitla',
        ciudad: 'Atlixco',
        estado: 'Pue.',
        codigoPostal: '74240'
      },
      calificaciones: [{
        nombre:"servicio",
        valoracion:4.7
      },
      {
        nombre: "atencion",
        valoracion:4.8
      },
      {
        nombre: "limpieza",
        valoracion:4.8
      }],
      tiempo: '20 min',
      distancia: '7 km',
      servicios: ['cafe', 'wifi', 'storefront']
    }
  ];


  constructor() { }
}
