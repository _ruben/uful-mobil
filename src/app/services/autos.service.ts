import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Auto } from '../interfaces';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { LoadingService } from './loading.service';
import { re } from 'mathjs';

@Injectable({
  providedIn: 'root'
})
export class AutosService {
  constructor(private http: HttpClient, private alertController : AlertController, private router : Router, private loading: LoadingService) { }
  baseUrl = 'https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/user/cars'

  autosObtenidos: Auto[] = [];
  poneFavoritoAlInicioDelArreglo() {

    const favoriteAutoIndex = this.autosObtenidos.findIndex(auto => auto.favorite);

      if (favoriteAutoIndex !== -1) {
        const favoriteAuto = this.autosObtenidos.splice(favoriteAutoIndex, 1)[0];
        this.autosObtenidos.unshift(favoriteAuto);
      }
    }
  async updateAutos () {
    this.autosObtenidos = [];
    console.log("autos vacio: ",this.autosObtenidos)
    this.getAutos().subscribe((data) => {
      console.log("Autos obtenidos: ", data);
      this.autosObtenidos = data;
      return this.autosObtenidos;
    }
    );
    return this.autosObtenidos;
  }

  async presentErrorAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Error!',
      subHeader: msg,
      buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
    });
    await alert.present();
  }
  async presentSuccessAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Éxito!',
      subHeader: msg,
      buttons: [{
        text: 'Aceptar',
        role: 'accept',
        handler: () => {
          this.router.navigate(['/tabs/perfil']);
        }
      }],
    });
    await alert.present();
  }
  public  getAutos(): Observable<any>{
    const url = this.baseUrl + '/getUserCars'
    const authToken = localStorage.getItem('token');
    console.log("Inicia petición de obtener autos");
    if( authToken == null){
      console.log("No hay token");
      return throwError("No hay token");
    }
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });

    return this.http.get(url, { headers }).pipe(
      map((response: any) => {
        console.log("Respuesta: ", response.data);
        this.autosObtenidos = response.data; //una vez que se obtienen los autos, se guardan en la variable autosObtenidos

        return this.autosObtenidos;
      }),
      catchError((error: any) => {
        console.log("Error: ", error);
        return throwError(error);
      })
    );
  }




  public postAuto(auto: Auto) {
    const body = {
      "brand": auto.brand,
      "car_model": auto.car_model,
      "year": auto.year,
      "license_plates": auto.license_plates,
      "fuel": auto.fuel,
      "nickname": auto.nickname,
      "state": auto.state,
      "favorite": auto.favorite,
      "color": auto.color
    }
    const authToken = localStorage.getItem('token');
    console.log("Inicia petición de autos");
    if( authToken == null){
      console.log("No hay token");
      return;
    }
    const url  = this.baseUrl + '/saveCar'
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });

    this.http.post(url, body, { headers }).subscribe(
      (response: any) => {
        console.log('Respuesta de la petición de guardar auto:', response);
        if (response.status === 200) {
          console.log("Éxito: ", response.msg);
          this.presentSuccessAlert(response.msg);
          return response.msg;
        }
      },
      (error: any) => {
        console.log('Error en la petición de autos:', error);
        this.presentErrorAlert(error.data.errors[1].message);
      }
    );

  }

  public putAuto(auto: Auto) {
    console.log("Auto a actualizar: ", auto);
    const body = {
      "brand": auto.brand,
      "car_model": auto.car_model,
      "year": auto.year,
      "license_plates": auto.license_plates,
      "fuel": auto.fuel,
      "nickname": auto.nickname,
      "state": auto.state,
      "favorite": auto.favorite,
      "color": auto.color
    }
    const authToken = localStorage.getItem('token');
    console.log("Inicia petición de autos");
    if( authToken == null){
      console.log("No hay token");
      return;
    }
    const url  = this.baseUrl + '/editCarProfile/' + auto._id
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    this.http.put(url, body, { headers }).subscribe(
      (response: any) => {
        console.log('Respuesta de la petición de actualizar auto:', response);
        if (response.status === 200) {
          console.log("Éxito: ", response.msg);
          this.presentSuccessAlert(response.msg);
          return response.msg;
        }
      },
      (error: any) => {
        console.log('Error en la petición de autos:', error);
        this.presentErrorAlert(error.data);
      }
    );
  }

  public deleteAuto(autoID: string){
    const url = this.baseUrl + '/deleteCar/' + autoID
    const authToken = localStorage.getItem('token');
    console.log("Inicia petición de eliminar auto");
    if( authToken == null){
      console.log("No hay token");
      return;
    }
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken});

    this.http.delete(url, { headers }).subscribe(
      (response: any) => {
        console.log('Respuesta de la petición de autos:', response);
        if (response.status === 200) {
          console.log("Éxito: ", response.msg);
          this.presentSuccessAlert(response.msg);
          return response.msg;
        }
      }
    );
  }

  public setFavorito(auto: any): Observable<any> {
    this.loading.showLoading("Guardando favorito");
    const url = this.baseUrl + '/setCarAsFavorite/' + auto._id;
    const authToken = localStorage.getItem('token');
    console.log("Inicia petición de set favorito autos");
    if (authToken == null) {
      console.log("No hay token");
      return of(null);
    }
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });

    return new Observable(observer => {
      this.http.put(url, {}, { headers }).subscribe(
        (response: any) => {
          console.log('Respuesta de la petición de autos:', response);
          if (response.status === 200) {
            console.log("Éxito: ", response.msg);
            this.loading.hideLoading();
            this.updateAutos();
            this.router.navigateByUrl('/mis-autos', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/mis-autos']);
            });
            observer.next(response.msg);
            observer.complete();
          }
        },
        (error: any) => {
          // Manejo del error
          observer.error(error);
        }
      );
    });
  }


}
