import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pedido } from '../interfaces';
import { Router } from '@angular/router';
import { PreciosService } from './precios.service';
import { AlertController } from '@ionic/angular';
import { QrService } from './qr.service';
import { Observable, throwError } from 'rxjs';
import { EventService } from './event.service';
import { LoadingService } from './loading.service';
import { RefreshHomeService } from './refresh-home.service';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  precios = []

  async presentErrorAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Error!',
      subHeader: msg,
      buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
    });
    await alert.present();
  }

  async presentSuccessAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Éxito!',
      subHeader: msg,
      buttons: [{
        text: 'Aceptar',
        role: 'accept',
        handler: () => {
          const url = '/tabs/carga';
          this.refreshService.triggerRefresh();
          this.router.navigateByUrl(url);
        }
      }],
    });
    await alert.present();
  }

  constructor(private http: HttpClient, private router: Router, private preciosServ :PreciosService, private alertController : AlertController, private qr : QrService, private eventoService : EventService, private loadingService : LoadingService, private refreshService: RefreshHomeService ) {
    this.preciosServ.getPrecios("645ab73ac407e908820ee64f")
  }


  public getTicket(carID: string): Observable<any> {
    const authToken = localStorage.getItem('token');
    if (authToken == null) {
      console.log("No hay token");
      return throwError("No hay token");
    }

    console.log("Obteniendo ticket...");

    const url = 'https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/user/cars/getTickets?car=' + carID;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });

    return this.http.get(url, { headers });
  }

  public createTicket( pedido: Pedido ) : boolean | void{
    let precioUful = 0;
    let fuel_id = "";
    if( pedido.carga.auto.fuel == "Regular"){
      precioUful = this.preciosServ.getPrecioMagna();
      fuel_id = "634d8512d2699c7099987a61";
    }
    else if( pedido.carga.auto.fuel == "Premium"){
      precioUful = this.preciosServ.getPrecioPremium();
      fuel_id = "634d8512d2699c7099987a62";
    }
    else if( pedido.carga.auto.fuel == "Diesel"){
      precioUful = this.preciosServ.getPrecioDiesel();
      fuel_id = "634d8512d2699c7099987a63";
    }
    const body = {
      "liters" : parseFloat(pedido.carga.litros),
      "price_per_liter": precioUful,
      "total": (precioUful * parseFloat(pedido.carga.litros)).toFixed(2),
      "fuel_id": fuel_id, // Regular termina 61 / Premium termina 62 / Diesel termina 63
      "gasStation_id": "6435cceb56d609d74b9138e7",
      "car_id": pedido.carga.auto._id,
      "express": pedido.carga.tipoDeServicio,
      // "liters":17,
      // "price_per_liter": 21.85,
      // "total":371.45,
      // "fuel_id":"634d8512d2699c7099987a62",
      // "gasStation_id": "6435cceb56d609d74b9138e7",
      // "car_id":"63f5c539d5f5f53050a6e738"
    }
    console.log("Body: ", body);
    const authToken = localStorage.getItem('token');
    if( authToken == null){
      console.log("No hay token");
      return;
    }

    console.log("Creando ticket...");

    const url = 'https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/user/cars/createNewTicket';
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });

    this.http.post(url, body, { headers }).subscribe(
      (response: any) => {
        const data = response.data;
        this.loadingService.hideLoading();
        this.presentSuccessAlert("Ticket creado para el vehículo: "+pedido.carga.auto.nickname);
        localStorage.setItem('autoID', pedido.carga.auto._id || '');
        this.qr.generateQRCode(data).then((qr) => {
          console.log("QR generado: ", qr);
          localStorage.setItem('qr', qr);
          console.log("respuetsa del qr almacenada en localStorage: ", localStorage.getItem('qr'));
          this.eventoService.updateData(true);
          return true;
        });
      },
      (error) => {
        this.loadingService.hideLoading();
        this.presentErrorAlert(`Error al crear ticket. ${error.error.data.errors[0].msg}`);
        console.log(error);
      }
    );




  }
}
