import { Injectable } from '@angular/core';
import { Direccion } from '../interfaces';
import { google } from 'google-maps';
import { GooglemapsService } from './googlemaps.service';
@Injectable({
  providedIn: 'root'
})
export class UbicacionService {

  userLocation?: [number, number];
  direccion: Direccion = {
    ubicacion: {
      lat: 0,
      lng: 0
    },
    ciudad: 'Puebla',
    estado: '',
    codigoPostal: ''
  } ;

  get isUserLocationReady(): boolean {
    return !!this.userLocation; // regresa true si existe un valor en userLocation y false si no existe
  }

  constructor() {

  }
  public async getUserLocation(): Promise<[number, number]> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        ({coords}) => {
          console.log("Geolocalizacion obtenida: ", coords);
          this.userLocation = [coords.latitude, coords.longitude];
          const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.userLocation[0]},${this.userLocation[1]}&key=AIzaSyCQiFNsJiQtKhd5MqKMtn3nqo91Pys6PsY`;
          fetch(url).then((response) => {
            response.json().then((data) => {
              this.direccion.calle = data.results[0].address_components[1].long_name;
              this.direccion.numero = data.results[0].address_components[0].long_name;
              this.direccion.colonia = data.results[0].address_components[2].long_name;
              this.direccion.ciudad = data.results[0].address_components[3].long_name;
              this.direccion.estado = data.results[0].address_components[4].long_name;
              this.direccion.codigoPostal = data.results[0].address_components[6]?.long_name || '';
              console.log("Direccion obtenida: ", this.direccion);
            });
          });
          resolve(this.userLocation);
        },
        (err) => {
          console.log("Error obteniendo geololaizacion: ", err);
          reject(err);
        }
      )
    });
  }

  getDireccion(){
    return this.direccion;
  }

  setDireccionFromGoogleMaps(result: google.maps.GeocoderResult){
    this.direccion.ubicacion.lat = result.geometry?.location?.lat() || 0;
    this.direccion.ubicacion.lng = result.geometry?.location?.lng() || 0;
    this.direccion.calle = result.address_components?.[1]?.long_name || 'null';
    this.direccion.numero = result.address_components?.[0]?.long_name || 'null';
    this.direccion.colonia = result.address_components?.[2]?.long_name || 'null';
    this.direccion.ciudad = result.address_components?.[3]?.long_name || 'null';
    this.direccion.estado = result.address_components?.[4]?.long_name || 'null';
    this.direccion.codigoPostal = result.address_components?.[6]?.long_name || 'null';
  }

}
