import { Injectable } from '@angular/core';
import { LoadingController, LoadingOptions } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loading: HTMLIonLoadingElement | null = null;

  constructor(private loadingController: LoadingController) {}

  async showLoading(text: string = 'Cargando...') {
    console.log('showLoading');
    if (!this.loading) {
      const options: LoadingOptions = {
        message: text,
        spinner: 'lines-sharp', // Utilizamos el spinner de tipo "circles"
        translucent: true,
        cssClass: 'custom-loading'
      };

      this.loading = await this.loadingController.create(options);
      await this.loading.present();
    }
  }

  async hideLoading() {
    console.log('hideLoading');
    if (this.loading !== null) {
      await this.loading.dismiss();
      this.loading = null;
    }
  }

}
