import { Injectable } from '@angular/core';
import { Pedido } from '../interfaces/index';

@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  pedido: Pedido = {
    estacion:{
      nombre: '',
      logo: '',
      direccion: {
        ubicacion: {
          lat: 0,
          lng: 0,
        },
        calle: '',
        numero: '',
        colonia: '',
        ciudad: '',
        estado: '',
        codigoPostal: '',
      },
      calificaciones: [],
      tiempo: '',
      distancia: '',
      servicios: [],
    },
    carga: {
      status: '',
      auto : {
        car_model: '', license_plates: '', fuel: '',
        year: '',
        nickname: '',
        state: '',
        brand: '',
        favorite: false
      },
      fecha: '',
      hora: '',
      total: '',
      litros: '',
      tipoDeServicio: true,
    }
  }

  constructor() { }

  getPedido(){
    return this.pedido;
  }
  updatePedido(pedido: Pedido){
    this.pedido = pedido;
  }

}
