import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }

  canActivate (): boolean {
    console.log("CanActivate actiado");
    if (localStorage.getItem('token')) {
      console.log("Existe token");
      return true;
    } else {
      console.log("No existe token");
      this.router.navigate(['/login']);
      return false;
    }
  }

}
