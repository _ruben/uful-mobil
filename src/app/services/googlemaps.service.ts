import { Injectable, Renderer2 } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Direccion } from '../interfaces';
declare var google: any;
@Injectable({
  providedIn: 'root'
})
export class GooglemapsService {



  apiKey = environment.mapsKey;
  mapLoaded = false;

  constructor() { }

  init(renderer: Renderer2, document: any): Promise<any> {
    return new Promise((resolve, reject) => {
      if(this.mapLoaded){
        console.log('Map already loaded');
        resolve(true);
        return;
      }
      const script = renderer.createElement('script');
      script.id = 'googleMaps';

      (window as any) ['mapInit'] = () => {
        this.mapLoaded = true;
        if(google){
          console.log('Google maps ready');
        }else{
          reject('Google maps not available');
        }
        resolve(true);
        return;
      }

      if(this.apiKey){
        script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
      }

      renderer.appendChild(document.body, script);
    })
  }

}
