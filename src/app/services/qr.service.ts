import { Injectable } from '@angular/core';
import * as QRCode from 'qrcode';

@Injectable({
  providedIn: 'root'
})
export class QrService {

  constructor() { }



  async generateQRCode(text: string): Promise<string> {
    try {
      const qrCodeString = await QRCode.toDataURL(text);
      const qrImage = qrCodeString;
      console.log('QR generado y guardado: ', qrImage);
      return qrImage;
    } catch (err) {
      console.error('Error al generar el código QR: ', err);
      return '';
    }
  }

}
