import { TestBed } from '@angular/core/testing';

import { RefreshHomeService } from './refresh-home.service';

describe('RefreshHomeService', () => {
  let service: RefreshHomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RefreshHomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
