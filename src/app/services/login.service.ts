import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router, private alertController: AlertController) { }

  async presentErrorAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Error!',
      subHeader: msg,
      buttons: [{text: 'Intentar de nuevo', role: 'cancel'}],
    });
    await alert.present();
  }

  public postLogin(email: string, password: string) : any{
    const url = 'https://o1moexabej.execute-api.us-east-1.amazonaws.com/dev/api/mobile/auth/singIn';
    const body = {
      email: email,
      password: password
    }
    console.log("iniciando sesión...");
    this.http.post(url, body, {
      headers: {
        'Content-Type': 'application/json',
      }
    }).subscribe(
      (response: any) => {
        console.log("respuesta exitosa:",response); // Maneja la respuesta exitosa
        console.log("user-data: ",response.data.user);
        localStorage.setItem("user-data", JSON.stringify(response.data.user));
        console.log("user-data guardado en local storage: ",localStorage.getItem('user-data'));
        localStorage.setItem('token', response.data.Token);
        console.log("token guardado en local storage: ",localStorage.getItem('token'));
        this.router.navigate(['/tabs/carga']);
        return true
      },
      (error: any) => {
        console.log("respuesta erronea:",error); // Maneja la respuesta erronea
        this.presentErrorAlert(error.error.data.errors[0].msg);
      }
    );
  }

}
