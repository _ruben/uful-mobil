export interface TicketDeCarga{
  _id:               string;
  ticket_folio:      null;
  liters:            number;
  liters_loaded:     number;
  exact_charge:      null;
  price_per_liter:   number;
  total:             number;
  total_charged:     number;
  car_id:            string;
  status:            string;
  customer_id:       string;
  ticket_number:     string;
  created_date?:      Date;
  created_time:      string;
  created_timestamp: number;
  createdAt?:         Date;
  updatedAt?:         Date;
  fuel:              Fuel;
}

export interface DatoFiscal {
  rfc: string;
  razonSocial: string;
  domicilioFiscal: string;
  regimenFiscal: string;
  usoCFDI: string;
}

export interface Fuel {
  _id:          string;
  name:         string;
  type:         string;
}

export interface Carga{
  status: string;
  auto: Auto
  fecha: string;
  hora: string;
  total: string;
  litros: string;
  tipoDeServicio?: boolean;
  precioActual?: number;
}

export interface Auto{
  _id?: string;
  year: string;
  nickname: string;
  color?: string;
  car_model: string; //es car model
  license_plates: string;
  fuel: string; //es el fuel en el json de la api
  state: string;
  brand: string;
  favorite: boolean;
}

export interface Estacion {
  nombre: string;
  logo: string;
  direccion: Direccion;
  calificaciones: Calificacion[];
  tiempo: string;
  distancia: string;
  servicios: string[];
}

export interface Calificacion {
  nombre: string;
  valoracion: number;
}

export interface Direccion {
  ubicacion: Ubicacion;
  calle?: string;
  numero?: string;
  colonia?: string;
  ciudad: string;
  estado: string;
  codigoPostal: string;
}

export interface Municipio {
  nombre: string;
  ubicacion: Ubicacion;
}

export interface Pedido {
  estacion: Estacion;
  carga: Carga;
}

export interface Ubicacion {
  lat: number;
  lng: number;
}

export interface Tarjeta {
  name: string;
  number: string;
  cvc: string;
  expiry: string;
  secretNumber: string;
  clave: string;
}

export interface User {
  email: string;
  name: string;
  photo: string;
  fiscal_data: FiscalData;
}

export interface FiscalData {
  RFC: string;
  domicilioFiscal: string;
  razonSocial: string;
  regimenFiscal: string;
  usoCFDI: string;
}

export interface ModelosPorMarca {
  [marca: string]: string[];
}
